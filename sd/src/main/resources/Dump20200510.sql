CREATE DATABASE  IF NOT EXISTS `erasmus` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `erasmus`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: erasmus
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accommodation`
--

DROP TABLE IF EXISTS `accommodation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `accommodation` (
  `id` varchar(255) NOT NULL,
  `distance_from_university` float DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `price_per_month` varchar(255) DEFAULT NULL,
  `rent_type` varchar(255) DEFAULT NULL,
  `address_id` varchar(255) DEFAULT NULL,
  `university_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6c5rb1wtweg3j1ah43fk8d51y` (`address_id`),
  KEY `FKj6ri0dbf05tjrwsma0lf5wqu0` (`university_id`),
  CONSTRAINT `FK6c5rb1wtweg3j1ah43fk8d51y` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FKj6ri0dbf05tjrwsma0lf5wqu0` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accommodation`
--

LOCK TABLES `accommodation` WRITE;
/*!40000 ALTER TABLE `accommodation` DISABLE KEYS */;
INSERT INTO `accommodation` VALUES ('1',5,'Arcadia Hall','500$','SHARED_DORM','1','1'),('2',4,'The Hatch','800$','SINGLE_ROOM','2','1');
/*!40000 ALTER TABLE `accommodation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `address` (
  `id` varchar(255) NOT NULL,
  `apartment_number` int(11) DEFAULT NULL,
  `street_name` varchar(255) DEFAULT NULL,
  `street_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES ('1',44,'LowerGlanmire Rd',4),('2',55,'Memo',3);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `admin` (
  `id` varchar(255) NOT NULL,
  `position` varchar(255) DEFAULT NULL,
  `room` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8ahhk8vqegfrt6pd1p9i03aej` (`user_id`),
  CONSTRAINT `FK8ahhk8vqegfrt6pd1p9i03aej` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('1','Secretary','55','3');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `application` (
  `id` varchar(255) NOT NULL,
  `creation_date` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `address_id` varchar(255) NOT NULL,
  `document_id` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `university_id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKk2iwah5esw22wuii22y5mgwn9` (`address_id`),
  KEY `FKi95jn4ckqwei6b60ul2i4guqa` (`document_id`),
  KEY `FKigm5jb0xdqnqjelaagm14dcva` (`student_id`),
  KEY `FK81cvj8vx6ijavy1noysu26ycg` (`university_id`),
  CONSTRAINT `FK81cvj8vx6ijavy1noysu26ycg` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`),
  CONSTRAINT `FKi95jn4ckqwei6b60ul2i4guqa` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`),
  CONSTRAINT `FKigm5jb0xdqnqjelaagm14dcva` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `FKk2iwah5esw22wuii22y5mgwn9` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` VALUES ('1','2020-03-02','APPROVED','1','1','1','1'),('2','2020-05-04','DENIED','1','1','2','1');
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `document` (
  `id` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
INSERT INTO `document` VALUES ('1','LEARNING_AGREEMENT');
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exam_result`
--

DROP TABLE IF EXISTS `exam_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `exam_result` (
  `id` varchar(255) NOT NULL,
  `grade` float DEFAULT NULL,
  `student_id` varchar(255) DEFAULT NULL,
  `subject_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqammetxqjhntvcu0d0iodbvhg` (`student_id`),
  KEY `FKedod2bleq6k8ljpkch8l24tmq` (`subject_id`),
  CONSTRAINT `FKedod2bleq6k8ljpkch8l24tmq` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  CONSTRAINT `FKqammetxqjhntvcu0d0iodbvhg` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exam_result`
--

LOCK TABLES `exam_result` WRITE;
/*!40000 ALTER TABLE `exam_result` DISABLE KEYS */;
INSERT INTO `exam_result` VALUES ('1',10,'1','1');
/*!40000 ALTER TABLE `exam_result` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grade_correspondence`
--

DROP TABLE IF EXISTS `grade_correspondence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `grade_correspondence` (
  `id` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grade_correspondence`
--

LOCK TABLES `grade_correspondence` WRITE;
/*!40000 ALTER TABLE `grade_correspondence` DISABLE KEYS */;
/*!40000 ALTER TABLE `grade_correspondence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobility`
--

DROP TABLE IF EXISTS `mobility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `mobility` (
  `id` varchar(255) NOT NULL,
  `accommodation_id` varchar(255) DEFAULT NULL,
  `application_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKr7nvh3lgqx72h9adi4t9xr9tn` (`accommodation_id`),
  KEY `FK1tglqyba68cdso4tg7y0ud6bm` (`application_id`),
  CONSTRAINT `FK1tglqyba68cdso4tg7y0ud6bm` FOREIGN KEY (`application_id`) REFERENCES `application` (`id`),
  CONSTRAINT `FKr7nvh3lgqx72h9adi4t9xr9tn` FOREIGN KEY (`accommodation_id`) REFERENCES `accommodation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobility`
--

LOCK TABLES `mobility` WRITE;
/*!40000 ALTER TABLE `mobility` DISABLE KEYS */;
INSERT INTO `mobility` VALUES ('1','1','1');
/*!40000 ALTER TABLE `mobility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speciality`
--

DROP TABLE IF EXISTS `speciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `speciality` (
  `id` varchar(255) NOT NULL,
  `speciality_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speciality`
--

LOCK TABLES `speciality` WRITE;
/*!40000 ALTER TABLE `speciality` DISABLE KEYS */;
INSERT INTO `speciality` VALUES ('1','COMPUTER_SCIENCE'),('2','AUTOMATION'),('3','ARCHITECTURE'),('4','TELECOMMUNICATIONS');
/*!40000 ALTER TABLE `speciality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speciality_subject`
--

DROP TABLE IF EXISTS `speciality_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `speciality_subject` (
  `speciality_id` varchar(255) NOT NULL,
  `subject_id` varchar(255) NOT NULL,
  PRIMARY KEY (`speciality_id`,`subject_id`),
  KEY `FKbvlrspwe5cj3t0fde37px7lyu` (`subject_id`),
  CONSTRAINT `FKbvlrspwe5cj3t0fde37px7lyu` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  CONSTRAINT `FKi1h4260k9cra32fo5a8u3shpf` FOREIGN KEY (`speciality_id`) REFERENCES `speciality` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speciality_subject`
--

LOCK TABLES `speciality_subject` WRITE;
/*!40000 ALTER TABLE `speciality_subject` DISABLE KEYS */;
INSERT INTO `speciality_subject` VALUES ('1','1'),('1','2'),('1','3'),('2','2');
/*!40000 ALTER TABLE `speciality_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `id` varchar(255) NOT NULL,
  `average_grade` float DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `year_of_study` int(11) DEFAULT NULL,
  `address_id` varchar(255) DEFAULT NULL,
  `speciality_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKcaf6ht0hfw93lwc13ny0sdmvo` (`address_id`),
  KEY `FKt434hun0i4tv58xnfvkbttk7v` (`speciality_id`),
  KEY `FKk5m148xqefonqw7bgnpm0snwj` (`user_id`),
  CONSTRAINT `FKcaf6ht0hfw93lwc13ny0sdmvo` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FKk5m148xqefonqw7bgnpm0snwj` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FKt434hun0i4tv58xnfvkbttk7v` FOREIGN KEY (`speciality_id`) REFERENCES `speciality` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES ('1',10,'1998-04-05',3,'1','1','1'),('2',5,'1996-02-02',4,'1','1','5');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject`
--

DROP TABLE IF EXISTS `subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `subject` (
  `id` varchar(255) NOT NULL,
  `credits` int(11) DEFAULT NULL,
  `subject_code` varchar(255) DEFAULT NULL,
  `subject_name` varchar(255) DEFAULT NULL,
  `term` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject`
--

LOCK TABLES `subject` WRITE;
/*!40000 ALTER TABLE `subject` DISABLE KEYS */;
INSERT INTO `subject` VALUES ('1',5,'CS4626','Functional Programming',1),('2',5,'CS4620','Software Engineering',2),('3',4,'CS50','Fundamental Algorithms',1);
/*!40000 ALTER TABLE `subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subject_mobility`
--

DROP TABLE IF EXISTS `subject_mobility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `subject_mobility` (
  `subject_id` varchar(255) NOT NULL,
  `mobility_id` varchar(255) NOT NULL,
  PRIMARY KEY (`subject_id`,`mobility_id`),
  KEY `FKoc83y2pkwk10fky1kpptcajee` (`mobility_id`),
  CONSTRAINT `FKal97r467xrfmglr7ws2atiqcj` FOREIGN KEY (`subject_id`) REFERENCES `subject` (`id`),
  CONSTRAINT `FKoc83y2pkwk10fky1kpptcajee` FOREIGN KEY (`mobility_id`) REFERENCES `mobility` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subject_mobility`
--

LOCK TABLES `subject_mobility` WRITE;
/*!40000 ALTER TABLE `subject_mobility` DISABLE KEYS */;
INSERT INTO `subject_mobility` VALUES ('1','1'),('2','1'),('3','1');
/*!40000 ALTER TABLE `subject_mobility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `teacher` (
  `id` varchar(255) NOT NULL,
  `department` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpb6g6pahj1mr2ijg92r7m1xlh` (`user_id`),
  CONSTRAINT `FKpb6g6pahj1mr2ijg92r7m1xlh` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES ('1','COMPUTER_SCIENCE','2'),('2','AUTOMATION','4');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `university`
--

DROP TABLE IF EXISTS `university`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `university` (
  `id` varchar(255) NOT NULL,
  `maximum_admitted_students` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `teacher_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdabemx4kux09n61j77523pm1e` (`teacher_id`),
  CONSTRAINT `FKdabemx4kux09n61j77523pm1e` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `university`
--

LOCK TABLES `university` WRITE;
/*!40000 ALTER TABLE `university` DISABLE KEYS */;
INSERT INTO `university` VALUES ('1',1,'University College Cork (UCC)','2'),('140c8ccf-7dc2-4c9b-8049-fc3de038863f',3,'UC Davies','1'),('2027e808-d96b-4e9a-a536-949a8b547b09',2,'UTCN','2');
/*!40000 ALTER TABLE `university` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `university_speciality`
--

DROP TABLE IF EXISTS `university_speciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `university_speciality` (
  `university_id` varchar(255) NOT NULL,
  `speciality_id` varchar(255) NOT NULL,
  PRIMARY KEY (`university_id`,`speciality_id`),
  KEY `FKai44devjia8dmv8o4xol06f94` (`speciality_id`),
  CONSTRAINT `FKai44devjia8dmv8o4xol06f94` FOREIGN KEY (`speciality_id`) REFERENCES `speciality` (`id`),
  CONSTRAINT `FKjc69ihuykdd7ibbr9tkc0v5fo` FOREIGN KEY (`university_id`) REFERENCES `university` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `university_speciality`
--

LOCK TABLES `university_speciality` WRITE;
/*!40000 ALTER TABLE `university_speciality` DISABLE KEYS */;
INSERT INTO `university_speciality` VALUES ('1','1'),('1','2'),('1','3');
/*!40000 ALTER TABLE `university_speciality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('1','georgi.marian@yahoo.com','Georgiana','Marian','$2a$10$nYAXu/l.pLOTEpG/rIUh7O/M4IEzPMYzEOv6FHI36Ovp8tncqGc6C','0756567034'),('2','cosminaivan@test.com','Cosmina','Ivan','123123','0755555555'),('3','oospiroska@test.com','Piroska','Oos','$2a$10$nYAXu/l.pLOTEpG/rIUh7O/M4IEzPMYzEOv6FHI36Ovp8tncqGc6C','0744444444'),('4','dmarian@test.com','Daniela','Marian','123123','0766666666'),('5','gigelfrone@test.com','Gigel','Frone','$2a$10$nYAXu/l.pLOTEpG/rIUh7O/M4IEzPMYzEOv6FHI36Ovp8tncqGc6C','0788888888');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-10 18:47:00
