package ro.utcn.sd.entity.enums;

public enum RentType {
    SHARED_DORM("Shared room"),
    SINGLE_ROOM("Single room"),
    HOST_FAMILY("Host family");

    private final String name;

    RentType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return this.name;
    }
}
