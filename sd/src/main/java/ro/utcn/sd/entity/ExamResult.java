package ro.utcn.sd.entity;

import javax.persistence.*;

@Entity
@Table(name = "exam_result")
public class ExamResult {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private Student student;

    @OneToOne
    private Subject subject;

    @Column
    private float grade;
}
