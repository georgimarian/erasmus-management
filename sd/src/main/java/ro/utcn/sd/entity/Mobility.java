package ro.utcn.sd.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="mobility")
public class Mobility {
    @Id
    private String id;

    @OneToOne
    @JoinColumn(name = "application_id")
    private Application application;

    @OneToOne
    @JoinColumn(name = "accommodation_id")
    private Accommodation accommodation;

    @ManyToMany(mappedBy = "mobility")
    private Set<Subject> subjects;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Accommodation getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }
}
