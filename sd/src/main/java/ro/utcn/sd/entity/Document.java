package ro.utcn.sd.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="document")
public class Document {

    @Id
    private String id;

    @Column
    private String type;

    @OneToMany(mappedBy = "document")
    private List<Application> application;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type ;
    }
}
