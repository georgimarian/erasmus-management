package ro.utcn.sd.entity;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="subject")
public class Subject {
    @Id
    private String id;

    @Column
    private String subjectCode;

    @Column
    private String subjectName;

    @Column
    private int credits;

    @Column
    private int term;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public Set<Mobility> getMobility() {
        return mobility;
    }

    public void setMobility(Set<Mobility> mobility) {
        this.mobility = mobility;
    }

    @ManyToMany(cascade = CascadeType.ALL )
    @JoinTable(name = "subject_mobility",
            joinColumns = @JoinColumn(name= "subject_id"),
            inverseJoinColumns = @JoinColumn(name  = "mobility_id"))
    private Set<Mobility> mobility;
}
