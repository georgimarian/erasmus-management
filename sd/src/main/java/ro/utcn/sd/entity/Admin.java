package ro.utcn.sd.entity;

import javax.persistence.*;

@Entity
@Table(name="admin")
public class Admin {
    @Id
    private String id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private ApplicationUser user;

    @Column
    private String position;

    @Column
    private String room;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser user) {
        this.user = user;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }
}
