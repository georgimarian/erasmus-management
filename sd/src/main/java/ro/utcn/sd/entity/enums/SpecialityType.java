package ro.utcn.sd.entity.enums;

public enum SpecialityType {
    ARCHITECTURE("Architecture"),
    AUTOMATION("Automation"),
    COMPUTER_SCIENCE("Computer Science"),
    TELECOMMUNICATIONS("Telecommunications"),
    MECHANICAL_ENGINEERING("Mechanical Engineering"),
    MACHINE_BUILDING("Machine Building");

    private final String name;

    SpecialityType(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return this.name;
    }


}
