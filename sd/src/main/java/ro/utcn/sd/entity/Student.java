package ro.utcn.sd.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="student")
public class Student {
    @Id
    private String id;

    @Column
    private Date dateOfBirth;

    @Column
    private int yearOfStudy;

    @Column
    private float averageGrade;

    @OneToOne
    @JoinColumn(name = "speciality_id")
    private Speciality speciality;

    @OneToOne
    @JoinColumn(name = "user_id")
    private ApplicationUser user;

    @OneToOne
    @JoinColumn( name = "address_id")
    private Address address = new Address();

    @OneToMany(mappedBy = "student")
    private List<Application> applications = new ArrayList<Application>();

    @OneToMany(mappedBy = "student")
    private List<ExamResult> examResults = new ArrayList<ExamResult>();

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public float getAverageGrade() {
        return averageGrade;
    }

    public void setAverageGrade(float averageGrade) {
        this.averageGrade = averageGrade;
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public List<ExamResult> getExamResults() {
        return examResults;
    }

    public void setExamResults(List<ExamResult> examResults) {
        this.examResults = examResults;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser user) {
        this.user = user;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }

    @Override
    public String toString() {
        return  user.toString() ;
    }
}
