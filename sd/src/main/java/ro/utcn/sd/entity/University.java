package ro.utcn.sd.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="university")
public class University {
    @Id
    private String id;

    @Column
    private String name;

    @Column
    private int maximumAdmittedStudents;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL )
    @JoinTable(name = "university_speciality",
    joinColumns = @JoinColumn(name= "university_id"),
    inverseJoinColumns = @JoinColumn(name  = "speciality_id"))
    private Set<Speciality> specialities;

    @JsonIgnore
    @OneToMany(mappedBy = "university")
    private List<Accommodation> accommodations;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaximumAdmittedStudents() {
        return maximumAdmittedStudents;
    }

    public void setMaximumAdmittedStudents(int maximumAdmittedStudents) {
        this.maximumAdmittedStudents = maximumAdmittedStudents;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Set<Speciality> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(Set<Speciality> specialities) {
        this.specialities = specialities;
    }

    public List<Accommodation> getAccommodations() {
        return accommodations;
    }

    public void setAccommodations(List<Accommodation> accommodations) {
        this.accommodations = accommodations;
    }

    @Override
    public String toString() {
        return "University{" +
                ", name='" + name + '\'' +
                ", maximumAdmittedStudents=" + maximumAdmittedStudents +
                ", teacher=" + teacher +
                ", specialities=" + specialities +
                ", accommodations=" + accommodations +
                '}';
    }
}
