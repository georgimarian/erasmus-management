package ro.utcn.sd.entity;

import javax.persistence.*;

@Entity
@Table(name="address")
public class Address {

        @Id
        private String id;

        @Column
        private String streetName;
        @Column
        private int streetNumber;
        @Column
        private int apartmentNumber;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public int getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(int apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }


    @Override
    public String toString() {
        return
                " St. " + streetName +
                ", no. " + streetNumber +
                ", ap. " + apartmentNumber ;
    }
}
