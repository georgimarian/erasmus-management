package ro.utcn.sd.entity;


import ro.utcn.sd.entity.enums.RentType;

import javax.persistence.*;

@Entity
@Table(name="accommodation")
public class Accommodation {
    @Id
    private String id;

    @Column
    private String name;

    @Column
    private String pricePerMonth;

    @Column
    private float distanceFromUniversity;

    @Column
    @Enumerated(EnumType.STRING)
    private RentType rentType;

    @ManyToOne
    @JoinColumn(name = "university_id")
    private University university;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPricePerMonth() {
        return pricePerMonth;
    }

    public void setPricePerMonth(String pricePerMonth) {
        this.pricePerMonth = pricePerMonth;
    }

    public float getDistanceFromUniversity() {
        return distanceFromUniversity;
    }

    public void setDistanceFromUniversity(float distanceFromUniversity) {
        this.distanceFromUniversity = distanceFromUniversity;
    }

    public RentType getRentType() {
        return rentType;
    }

    public void setRentType(RentType rentType) {
        this.rentType = rentType;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
