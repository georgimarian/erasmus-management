package ro.utcn.sd.entity;

import ro.utcn.sd.entity.enums.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="application")
public class Application {

    @Id
    private String id;

    @Column
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column
    @Temporal(TemporalType.DATE)
    private java.util.Date creationDate;

    @OneToOne
    @JoinColumn(name = "university_id", nullable = false)
    private University university;

    @ManyToOne
    @JoinColumn(name="document_id", nullable = false)
    private Document document;

    @ManyToOne
    @JoinColumn(name="student_id", nullable = false)
    private Student student;

    @OneToOne
    @JoinColumn(name="address_id", nullable = false)
    private Address address;

    public Application(){
        this.id = UUID.randomUUID().toString();
        this.status = Status.PENDING;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    @Override
    public String toString() {
        return "Application{" +
                "id='" + id + '\'' +
                ", status=" + status +
                ", creationDate=" + creationDate +
                ", university=" + university +
                ", document=" + document +
                ", student=" + student +
                ", address=" + address +
                '}';
    }
}
