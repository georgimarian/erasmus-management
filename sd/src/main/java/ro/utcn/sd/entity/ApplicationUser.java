package ro.utcn.sd.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "user")
public class ApplicationUser {

    @Id
    private String id;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;

    @Column
    private String password;

    @Column
    private String phoneNumber;

    public ApplicationUser() {

	}

    private ApplicationUser(ApplicationUserBuilder builder){
    	this.id = builder.id;
    	this.firstName = builder.firstName;
    	this.lastName = builder.lastName;
    	this.email = builder.email;
    	this.password = builder.password;
    	this.phoneNumber = builder.phoneNumber;
	}

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }

    public static class ApplicationUserBuilder {
        private String id;
        private String firstName;
        private String lastName;
        private String email;
        private String password;
        private String phoneNumber;

        public ApplicationUserBuilder(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = UUID.randomUUID().toString();

        }

        public ApplicationUserBuilder email(String email) {
            this.email = email;
            return this;
        }

        public ApplicationUserBuilder password(String password) {
            this.password = password;
            return this;
        }

        public ApplicationUserBuilder phoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        //Return the finally constcuted User object
        public ApplicationUser build() {
            ApplicationUser citizen = new ApplicationUser(this);
            //validateUserObject(citizen);
            return citizen;
        }
    }
}
