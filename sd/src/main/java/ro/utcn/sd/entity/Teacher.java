package ro.utcn.sd.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import ro.utcn.sd.entity.enums.SpecialityType;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="teacher")
public class Teacher {
    @Id
    private String id;

    @Column
    @Enumerated(EnumType.STRING)
    private SpecialityType department;

    @JsonIgnore
    @OneToMany(mappedBy = "teacher")
    private List<University> universities;

    @OneToOne
    @JoinColumn(name = "user_id")
    private ApplicationUser user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SpecialityType getDepartment() {
        return department;
    }

    public void setDepartment(SpecialityType department) {
        this.department = department;
    }

    public List<University> getUniversities() {
        return universities;
    }

    public void setUniversities(List<University> universities) {
        this.universities = universities;
    }

    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return user.getFirstName() + " " +
                user.getLastName() + ", " +
                "department of: " +
                department.toString();
    }
}
