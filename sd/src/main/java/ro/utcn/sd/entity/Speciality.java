package ro.utcn.sd.entity;


import ro.utcn.sd.entity.enums.SpecialityType;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="speciality")
public class Speciality {
    @Id
    private String id;

    @Column
    @Enumerated(EnumType.STRING)
    private SpecialityType specialityType;

    @ManyToMany(mappedBy = "specialities")
    private Set<University> universities;

    @ManyToMany(cascade = CascadeType.ALL )
    @JoinTable(name = "speciality_subject",
            joinColumns = @JoinColumn(name= "speciality_id"),
            inverseJoinColumns = @JoinColumn(name  = "subject_id"))
    private Set<Subject> subjects;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SpecialityType getSpecialityType() {
        return specialityType;
    }

    public void setSpecialityType(SpecialityType specialityType) {
        this.specialityType = specialityType;
    }

    public Set<University> getUniversities() {
        return universities;
    }

    public void setUniversities(Set<University> universities) {
        this.universities = universities;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }
}
