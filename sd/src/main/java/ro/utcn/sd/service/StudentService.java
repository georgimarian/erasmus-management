package ro.utcn.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.repository.StudentRepository;

import java.util.List;
import java.util.UUID;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    /**
     * Get a Student by its UUID
     *
     * @param id - UUID to be found
     * @return - corresponding Student, if it exists
     */
    public Student getStudentById(String id) {
        try {
            Student student = studentRepository.getStudentById(id);
            return student;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Get Student by its ApplicationUser
     *
     * @param user - ApplicationUser to be found
     * @return - corresponding Student, if it exists
     */
    public Student getStudentByUser(ApplicationUser user) {
        try {
            return studentRepository.getStudentByUser(user);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Get all Student entities in the database in a list
     *
     * @return -  List of Student
     */
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    /**
     * Delete a Student
     *
     * @param student - Student to be deleted
     */
    public void deleteStudent(Student student) {
        studentRepository.delete(student);
    }

    /**
     * Save Student in the database
     *
     * @param student
     * @return
     */
    public Student save(Student student) {
        try {
            student.setId(UUID.randomUUID().toString());
            return studentRepository.save(student);
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

}
