package ro.utcn.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.University;
import ro.utcn.sd.repository.UniversityRepository;

import java.util.List;

@Service
public class UniversityService {
    @Autowired
    private UniversityRepository universityRepository;

    /**
     * Get a University by its UUID
     * @param id - UUID to be found
     * @return - corresponding University
     */
    public University getUniversityById(String id) {
        return universityRepository.findUniversityById(id);
    }

    /**
     * Edit a University
     * @param university - University to be edited
     * @return - the edited University
     */
    public University editUniversity(University university) {
        return universityRepository.save(university);
    }

    /**
     * Delete a University
     * @param university - University to be deleted
     * @return - boolean value for success or failure
     */
    public boolean deleteUniversity(University university) {
        try {
            universityRepository.delete(university);
            return true;
        } catch (Exception e){
            return false;
        }
    }

    public List<University> getAllUniversities() {
        return universityRepository.findAll();
    }

    public University saveUniversity(University university) {
        return universityRepository.save(university);
    }
}
