package ro.utcn.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.Document;
import ro.utcn.sd.repository.DocumentRepository;

@Service
public class DocumentService {
    @Autowired
    private DocumentRepository documentRepository;

    public Document getDocumentById(String id){
        return documentRepository.getDocumentById(id);
    }
}
