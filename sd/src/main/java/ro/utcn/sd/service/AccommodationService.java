package ro.utcn.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.Accommodation;
import ro.utcn.sd.entity.University;
import ro.utcn.sd.repository.AccommodationRepository;

import java.util.List;

@Service
public class AccommodationService {
    @Autowired
    private AccommodationRepository accommodationRepository;

    /**
     * Get Accomodation entities by their corrresponding University
     * @param university - University entity
     * @return - list of Accomodation having the associated University
     */
    public List<Accommodation> getAccommodationsByUniversity(University university) {
        return accommodationRepository.getAccommodationsByUniversity(university);
    }

    /**
     * Add accommodation to database
     * @param accommodation - accommodation to be added
     * @return - added accommodation
     */
    public Accommodation add(Accommodation accommodation){
        return accommodationRepository.save(accommodation);
    }
}
