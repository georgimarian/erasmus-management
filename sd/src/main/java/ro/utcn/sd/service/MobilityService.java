package ro.utcn.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.entity.Mobility;
import ro.utcn.sd.repository.MobilityRepository;

import java.util.List;

@Service
public class MobilityService {
    @Autowired
    private MobilityRepository mobilityRepository;

    /**
     * Get a List of all Mobilities
     * @return - list of all Mobility entities in the database
     */
    public List<Mobility> getAllMobilities() {
        return mobilityRepository.findAll();
    }

    /**
     * Get Mobility by its Application
     * @param application - Application to be found inside a Mobility
     * @return - the corresponding Mobility
     */
    public Mobility getMobilityByApplication(Application application) {
        return mobilityRepository.findMobilityByApplication(application);
    }

    /**
     * Get a Mobility by its UUID
     * @param id - UUID to be found
     * @return - corresponding Mobility entity
     */
    public Mobility getMobilityById(String id) {
        return mobilityRepository.findMobilityById(id);
    }

    /**
     * Update a Mobility
     * @param mobility - Mobility to be updated
     * @return - the updated Mobility
     */
    public Mobility editMobility(Mobility mobility) {
        return mobilityRepository.save(mobility);
    }
}
