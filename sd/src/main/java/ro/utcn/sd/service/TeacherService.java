package ro.utcn.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.Teacher;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.repository.TeacherRepository;
import ro.utcn.sd.repository.ApplicationUserRepository;

import java.util.List;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private ApplicationUserRepository userRepository;

    public List<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }

    /**
     * Find a Teacher by his name
     * @param name - name to be found
     * @return - corresponding Teacher
     */
    public Teacher findByName(String name) {
        String[] names = name.split("\\s+");
        ApplicationUser user = userRepository.findByFirstNameAndLastName(names[0], names[1]);
        return teacherRepository.findTeacherByUser(user);
    }

    /**
     * Find Teacher by its UUID
     * @param id - UUID to be found
     * @return - corresponding Teacher, if it exists
     */
    public Teacher findById(String id) {
        try {
            return teacherRepository.findTeacherById(id);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Find Teacher by ApplicationUser
     * @param user - ApplicationUser to be found
     * @return - Teacher, if it exists
     */
    public Teacher findByUser(ApplicationUser user) {
        try {
            return teacherRepository.findTeacherByUser(user);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Delete a Teacher from the database
     * @param teacher - teacher to be deleted
     * @throws Exception
     */
    public void deleteTeacher(Teacher teacher) throws Exception {
        try {
            teacherRepository.delete(teacher);
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
}
