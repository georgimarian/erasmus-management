package ro.utcn.sd.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ro.utcn.sd.util.constants.Role;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.entity.Teacher;
import ro.utcn.sd.repository.ApplicationUserRepository;

import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC256;
import static java.util.Objects.isNull;

@Service
public class ApplicationUserService {
    @Autowired
    private ApplicationUserRepository userRepository;

    @Autowired
    private StudentService studentService;

    @Autowired
    private TeacherService teacherService;

    /**
     * Method that finds an ApplicationUser by e-mail and password
     *
     * @param email - ApplicationUser's e-mail
     * @param password - ApplicationUser's password
     * @return the corresponding ApplicationUser, if it exists
     */
    public ApplicationUser findByEmailAndPassword(String email, String password) {
        try {
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            ApplicationUser user = userRepository.findByEmail(email);
            if (!passwordEncoder.matches(password, user.getPassword())) {
                return null;
            }
            return user;
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }
    }

    /**
     * Method that finds an ApplicationUser by e-mail
     * @param email - e-mail to be found
     * @return ApplicationUser, if it exists
     */
    public ApplicationUser findByEmail(String email) {
        try {
            return userRepository.findByEmail(email);
        }catch (Exception e){
            return null;
        }
    }

    /**
     * Create an ApplicationUser in the database
     * @param user - user to be added to the database
     * @return - created ApplicationUser
     */
    public ApplicationUser save(ApplicationUser user) {
        return userRepository.save(user);
    }

    /**
     * Method that gets the role of an ApplicationUser
     * @param user - ApplicationUser whose role needs to be found
     * @return - the role, as String
     */
    public String findUserRole(ApplicationUser user) {
        Student student = studentService.getStudentByUser(user);
        if (isNull(student)) {
            Teacher teacher = teacherService.findByUser(user);
            if (isNull(teacher)) {
                return Role.ADMIN.toString();
            }
            return Role.TEACHER.toString();
        }
        return Role.STUDENT.toString();
    }

    /**
     * Method that generates a JWT token
     * @param user - ApplicationUser used for token generation
     * @return - JWT token, as string
     */
    public String generateToken(ApplicationUser user) {
        String token = JWT.create()
                .withIssuer("auth0")
                .withSubject(user.getEmail())
                .withClaim("role", findUserRole(user))
                .withClaim("name", user.getFirstName() + ' ' + user.getLastName())
                .withExpiresAt(new Date(System.currentTimeMillis() + 864_000_000)) //10 days
                .sign(HMAC256("SecretKeyToGenJWTs"));
        return token;
    }

    /**
     * Method that gets the bearer of a JWT token
     * @param token - JWT token to be decoded
     * @return - corresponding ApplicationUser
     */
    public ApplicationUser getTokenBearer(String token) {
        Algorithm algorithm = Algorithm.HMAC256("SecretKeyToGenJWTs");
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("auth0")
                .build();
        DecodedJWT jwt = verifier.verify(token);
        String subject = jwt.getSubject();
        ApplicationUser user = findByEmail(subject);
        return user;
    }

    /**
     * Method that generates a temporary password for password reset
     * @param user - user for which temporary password is generated
     * @return - generated password
     */
    public String resetPassword(ApplicationUser user) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String randomPassword = RandomStringUtils.randomAlphanumeric(10);
        user.setPassword(passwordEncoder.encode(randomPassword));
        userRepository.save(user);
        return randomPassword;
    }

}
