package ro.utcn.sd.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.repository.ApplicationRepository;

import java.util.List;

@Service
public class ApplicationService {
    @Autowired
    private ApplicationRepository applicationRepository;

    /**
     * Get all Application entities
     * @return - list of Application
     */
    public List<Application> getAllApplications() {
        return applicationRepository.findAll();
    }

    /**
     * Get an Application by its UUID
     * @param id - UUID to be found
     * @return - Application with the corresponding UUID
     */
    public Application getApplicationById(String id) {
        return applicationRepository.findApplicationById(id);
    }

    /**
     * Get an Application by a Student
     * @param student - associated Student entity
     * @return - Application having the Student as attribute
     */
    public List<Application> getApplicationsByStudent(Student student) {
        return applicationRepository.findApplicationsByStudent(student);
    }

    /**
     * Update content of an Application
     * @param application - Application to be updated
     */
    public void editApplication(Application application) {
        applicationRepository.save(application);
    }

    /**
     * Add an application
     * @param application - Application to be added
     */
    public void addApplication(Application application) { applicationRepository.save(application);}

    /**
     * Delete an Application
     * @param application - Application to be deleted
     * @return - boolean value meaning success or failure
     */
    public boolean deleteApplication(Application application) {
        try {
            applicationRepository.delete(application);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
