package ro.utcn.sd.util.strategy;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import ro.utcn.sd.entity.Application;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class ExportCsv implements Strategy {
    /**
     * Export a list to CSV using the Strategy pattern
     * @param application
     * @return
     */
    @Override
    public boolean export(Application application) {

        try {
            String fileName = application.getStudent().getUser().getFirstName() +
                    application.getStudent().getUser().getLastName() +
                    application.getCreationDate().toString();
            FileWriter out = new FileWriter( fileName + ".csv");

            CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT
                .withHeader("date", "type", "sum"));
            List<Application> applications = new ArrayList<>();
            applications.add(application);
            for(Application application1: applications) {
                printer.printRecord(
                        application1.getCreationDate(),
                        application1.getStatus().toString(),
                        application1.getUniversity().toString());
            }
            printer.close();
        }catch(Exception e){
            return false;
        }
        return true;
    }
}
