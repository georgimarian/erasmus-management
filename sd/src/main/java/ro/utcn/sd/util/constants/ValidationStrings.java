package ro.utcn.sd.util.constants;

/**
 * Class containing Validation Messages
 */
public class ValidationStrings {
    public static final String INVALID_TOKEN = "Invalid token";
    public static final String INVALID_CREDENTIALS = "Invalid credentials";
    public static final String INVALID_CREDENTIALS_ATTEMPT = "Attempted to use invalid credentials";
    public static final String SUCCESS_CREATE_TOKEN = "Created token successfully!";
    public static final String ERROR_LOGIN = "Error logging in";
    public static final String ERROR_EDIT = "Error editing ";
    public static final String CANNOT_DELETE = "Cannot delete! Dependencies exist!";
    public static final String CANNOT_ADD_EMAIL = "Cannot add user with same e-mail address";
    public static final String EMAIL_EXISTS = "User with this email already exists!";
    public static final String EMAIL_NOT_EXISTS = "User with this email does not exist!";

}
