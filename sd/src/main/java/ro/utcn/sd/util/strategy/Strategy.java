package ro.utcn.sd.util.strategy;

import ro.utcn.sd.entity.Application;

/**
 * Interface used for the implementation of the Strategy pattern
 */
public interface Strategy {
    public boolean export(Application application);
}
