package ro.utcn.sd.util.strategy;


import ro.utcn.sd.entity.Application;

/**
 * Context class for the implementation of the Strategy Pattern
 */
public class Context {
    private Strategy strategy;

    /**
     * Creates context according to the used strategy
     * @param strategy
     */
    public Context(Strategy strategy){
        this.strategy = strategy;
    }

    public boolean executeStrategy(Application application){
        return strategy.export(application);
    }
}
