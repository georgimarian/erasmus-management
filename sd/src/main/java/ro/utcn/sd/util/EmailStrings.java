package ro.utcn.sd.util;

/**
 * Class containing message constants
 */
public class EmailStrings {
    public static final String NEW_APPLICATION = "New Erasmus Application";

    public static final String ENDING = "\nKind regards,\n" +
            "Erasmus+ Team";

    public static final String NEW_APPLICATION_BODY = "You can find your application summary attached below.\n" +
            "Contact us should the information be incorrect" +
            "Please remember to frequently check your account for any updates \n" +
            "Thank you for choosing to apply with us!\n" +
            ENDING;

    public static  final String REJECTED_APPLICATION = "Rejected application";

    public static final String APPLICATION_DENIED = "Your application has been rejected by the review commission! \n " +
            "Please consider initiating another application.\n" +
            ENDING;

    public static final String WELCOME = "Welcome to Erasmus Manager";

    public static final String REGISTER_SUCCESS = "You have successfully registered as an Erasmus+ student\n" +
            "Please access your account at localhost:4200\\login to create applications\n" +
            ENDING;

    public static final String APPLICATION_SUMMARY = "Application Summary";

    public static final String RESET_PASSWORD = "Password reset";

    public static final String PASSWORD_CHANGED = "Your password has been changed!\n" +
            "You can now login using your new password!" +
            ENDING;

    public static final String FAIL = "FAILED AUTHENTICATION";

    public static final String BODY_FAIL = "You have entered wrong credentials three times! Your account is temporarily blocked";
}
