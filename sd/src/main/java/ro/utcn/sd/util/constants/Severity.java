package ro.utcn.sd.util.constants;

/**
 * Enum for the Severity of the Response messages
 */
public enum Severity {
    SUCCESS("SUCCESS"),
    ERROR("ERROR"),
    WARNING("WARNING");

    private final String name;

    Severity(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return this.name;
    }
}
