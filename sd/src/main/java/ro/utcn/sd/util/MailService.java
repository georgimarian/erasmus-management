package ro.utcn.sd.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.util.strategy.Context;
import ro.utcn.sd.util.strategy.ExportPdf;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Service
public class MailService {

    @Autowired
    private JavaMailSender javaMailSender;

    /**
     * Builds an e-mail
     *
     * @param to      - destination
     * @param subject - title
     * @param body    - content
     * @return a SimpleMailMessage configured using the above parameters
     */
    private SimpleMailMessage setSimpleMailMessage(String to, String subject, String body) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("119112833@umail.ucc.ie");
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(body);
        return simpleMailMessage;
    }

    /**
     * Performs the e-mail sending
     *
     * @param to      - destination of the e-mail
     * @param subject - title of the e-mail
     * @param body    - content of the e-mail
     */
    public void sendMail(String to, String subject, String body) {
        try {
            SimpleMailMessage simpleMailMessage = setSimpleMailMessage(to, subject, body);
            javaMailSender.send(simpleMailMessage);
        } catch (Exception e) {
            e.toString();
        }
    }

    /**
     * Function that sends a bill by e-mail
     *
     * @param to          - destination e-mail
     * @param subject     - subject of the mail
     * @param body        - body of the e-mail
     * @param application - the application to be sent as a file
     */
    public void sendApplication(String to, String subject, String body, Application application) {
        SimpleMailMessage simpleMailMessage = setSimpleMailMessage(to, subject, body);

        MimeMessage message = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom(simpleMailMessage.getFrom());
            helper.setTo(simpleMailMessage.getTo());
            helper.setSubject(simpleMailMessage.getSubject());
            helper.setText(String.format(
                    simpleMailMessage.getText(), subject, body));

            Context context = new Context(new ExportPdf());
            context.executeStrategy(application);
            FileSystemResource file = new FileSystemResource(
                    "C:\\Users\\georg\\Desktop\\software-design-2020-30433-assignment3-georgimarian\\sd\\" + application.getStudent().getUser().getFirstName() +
                            application.getStudent().getUser().getLastName() +
                            application.getId() + ".pdf");
            helper.addAttachment(file.getFilename(), file);

        } catch (MessagingException e) {
            System.out.print(e.toString());
        }
        javaMailSender.send(message);
    }

}
