package ro.utcn.sd.util.constants;

/**
 * Enum for the ApplicationUser roles
 */
public enum Role {
    ADMIN("admin"),
    STUDENT("student"),
    TEACHER("teacher");

    private final String name;

    Role(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return this.name;
    }
}
