package ro.utcn.sd.util.constants;

/**
 * Class containing Response Strings
 */
public class ResponseStrings {
    /**
     * University Utility Strings
     */
    public static final String ADD_UNIVERSITY_SUCCESS = "Added University successfully";
    public static final String ADD_UNIVERSITY_ERROR = "Error adding University";

    public static final String GET_UNIVERSITY_SUCCESS = "Got University successfully";
    public static final String GET_UNIVERSITIES_SUCCESS = "Got Universities successfully";
    public static final String GET_UNIVERSITY_ERROR = "Error getting University";
    public static final String GET_UNIVERSITIES_ERROR = "Error getting Universities";

    public static final String EDIT_UNIVERSITY_SUCCESS = "Edited University successfully";
    public static final String EDIT_UNIVERSITY_ERROR = "Error getting University";

    public static final String DELETE_UNIVERSITY_SUCCESS = "Deleted University successfully";
    public static final String DELETE_UNIVERSITY_ERROR = "Error deleting University ";

    /**
     * Application Utility Strings
     */

    public static final String ADD_APPLICATION_SUCCESS = "Added Application successfully";
    public static final String ADD_APPLICATION_ERROR = "Error adding Appliction";

    public static final String GET_APPLICATION_SUCCESS = "Got Application successfully";
    public static final String GET_APPLICATIONS_SUCCESS = "Got Applications successfully";
    public static final String GET_APPLICATION_ERROR = "Error getting Application";
    public static final String GET_APPLICATIONS_ERROR = "Error getting Applications";

    public static final String EDIT_APPLICATION_SUCCESS = "Edited Application successfully";
    public static final String EDIT_APPLICATION_ERROR = "Error getting Application";

    public static final String DELETE_APPLICATION_SUCCESS = "Deleted Application successfully";
    public static final String DELETE_APPLICATION_ERROR = "Error deleting Application ";

    public static final String CANNOT_DELETE_ONGOING = "Cannot be deleted since it is ongoing!";

    public static final String CANNOT_DELETE_HAS_MOBILITIES = "Cannot delete since it has current mobilities!";

    /**
     * Accommodation Utility Strings
     */
    public static final String ADD_ACCOMMODATION_SUCCESS = "Added Accommodation successfully";
    public static final String ADD_ACCOMMODATION_ERROR = "Error adding Accommodation";

    /**
     * Student Utility Strings
     */
    public static final String ADD_STUDENT_SUCCESS = "Added Student successfully";
    public static final String ADD_STUDENT_ERROR = "Error adding Student";

    public static final String GET_STUDENT_SUCCESS = "Got Student successfully";
    public static final String GET_STUDENTS_SUCCESS = "Got Students successfully";
    public static final String GET_STUDENT_ERROR = "Error getting Student";
    public static final String GET_STUDENTS_ERROR = "Error getting Students";

    public static final String EDIT_STUDENT_SUCCESS = "Edited Student successfully";
    public static final String EDIT_STUDENT_ERROR = "Error getting Students";

    public static final String DELETE_STUDENT_SUCCESS = "Deleted Student successfully";
    public static final String DELETE_STUDENT_ERROR = "Error deleting Students ";

    /**
     * Teacher Utility Strings
     */
    public static final String ADD_TEACHER_SUCCESS = "Added Teacher successfully";
    public static final String ADD_TEACHER_ERROR = "Error adding Teacher";

    public static final String GET_TEACHER_SUCCESS = "Got Teacher successfully";
    public static final String GET_TEACHERS_SUCCESS = "Got Teacher successfully";
    public static final String GET_TEACHER_ERROR = "Error getting Teacher";
    public static final String GET_TEACHERS_ERROR = "Error getting Teacher";

    public static final String EDIT_TEACHER_SUCCESS = "Edited Teacher successfully";
    public static final String EDIT_TEACHER_ERROR = "Error editing Teacher";

    public static final String DELETE_TEACHER_SUCCESS = "Deleted Teacher successfully";
    public static final String DELETE_TEACHER_ERROR = "Error deleting Teacher ";

    /**
     * Mobility Utility Strings
     */
    public static final String ADD_MOBILITY_SUCCESS = "Added Mobility successfully";
    public static final String ADD_MOBILITY_ERROR = "Error adding Mobility";

    public static final String GET_MOBILITY_SUCCESS = "Got Mobility successfully";
    public static final String GET_MOBILITIES_SUCCESS = "Got Mobilities successfully";
    public static final String GET_MOBILITY_ERROR = "Error getting Mobility";
    public static final String GET_MOBILITIES_ERROR = "Error getting Mobilities";

    public static final String EDIT_MOBILITY_SUCCESS = "Edited Mobility successfully";
    public static final String EDIT_MOBILITY_ERROR = "Error getting Mobility";

    public static final String DELETE_MOBILITY_SUCCESS = "Deleted Mobility successfully";
    public static final String DELETE_MOBILITY_ERROR = "Error deleting Mobility ";

    /**
     * Accommodation Utility Strings
     */
    public static final String GET_ACCOMMODATION_SUCCESS = "Got accommodation for university successfully!";
    public static final String GET_ACCOMMODATION_ERROR = "Error getting accommodations!";

    /**
     * Message Utility Strings
     */
    public static final String MESSAGE_SENT = "Sent message successfully";
    public static final String MESSAGE_ERROR = "Error sending message";


    /**
     * Registration Utility Strings
     */

    public static final String REGISTER_SUCCESS = "Registered Successfully!";
    public static final String REGISTER_ERROR = "Error registering!";

    public static final String CHANGE_PASSWORD_SUCCESS = "Password changed!";
    public static final String CHANGE_PASSWORD_ERROR = "Error changing password!";

    public static final String RESET_BODY = "Your password can be reset on localhost:4200/reset-code using the code \n";

}
