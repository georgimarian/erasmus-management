package ro.utcn.sd.util.strategy;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.util.EmailStrings;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class ExportPdf implements Strategy {
    /**
     * Exports the list to PDF using strategy pattern
     * @param application  to be exported
     * @return boolean value to tell if successful or not
     */
    @Override
    public boolean export(Application application) {
        Document document = new Document();
        try{
            String fileName = application.getStudent().getUser().getFirstName() +
                    application.getStudent().getUser().getLastName() +
                    application.getId() + ".pdf";
            
            PdfWriter.getInstance(document, new FileOutputStream(fileName));

            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            Chunk chunk = new Chunk(EmailStrings.APPLICATION_SUMMARY, font);
            document.add(chunk);
            document.add(new Phrase("\n"));

            PdfPTable table = new PdfPTable(3);
            addTableHeader(table);
            List<Application> applications = new ArrayList<>();
            applications.add(application);
            addRows(table, applications);
            document.add(table);
            document.close();
        }catch(Exception e){
            return false;
        }
        return true;
    }

    /**
     * Adds a table header to a PDF table
     * @param table
     */
    private void addTableHeader(PdfPTable table) {
        Stream.of("date", "university", "status")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    /**
     * Adds all rows to a pdf table
     * @param table where the rows are added
     * @param applications list of transactions for the corresponding rows
     */
    private void addRows(PdfPTable table, List<Application> applications) {
        for(Application application : applications){
            table.addCell(application.getCreationDate().toString());
            table.addCell(application.getUniversity().toString());
            table.addCell(String.valueOf(application.getStatus()));
        }
    }
}
