package ro.utcn.sd.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.teacher.TeacherDto;
import ro.utcn.sd.entity.Teacher;
import ro.utcn.sd.service.TeacherService;

@Component
public class TeacherDtoMapper {
    @Autowired
    private TeacherService teacherService;

    public static TeacherDto convertToDto(Teacher teacher) {
        TeacherDto teacherDto = new TeacherDto();
        teacherDto.setId(teacher.getId());
        teacherDto.setUser(teacher.getUser());
        teacherDto.setDepartment(teacher.getDepartment().toString());
        return teacherDto;
    }

    public Teacher convertToEntity(TeacherDto teacherDto) {
        return teacherService.findByUser(teacherDto.getUser());
    }
}
