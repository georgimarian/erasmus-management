package ro.utcn.sd.mapper;

import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.accommodation.AccommodationDto;
import ro.utcn.sd.entity.Accommodation;

/**
 * Class that maps Accommodation to AccommodationDto and vice versa
 */
@Component
public class AccommodationDtoMapper {

    /**
     * Convert an AccommodationDto to Accommodation
     * @param accommodationDto - DTO to be converted
     * @return - converted Accommodation
     */
    public Accommodation convertToEntity(AccommodationDto accommodationDto){
        Accommodation accommodation = new Accommodation();
        accommodation.setDistanceFromUniversity(accommodationDto.getDistanceFromUniversity());
        accommodation.setName(accommodationDto.getName());
        accommodation.setPricePerMonth(accommodationDto.getPricePerMonth());
        accommodation.setRentType(accommodationDto.getRentType());
        return accommodation;
    }

    /**
     * Convert an Accommodation to AccommodationDTO
     * @param accommodation - the Accommodation to be converted
     * @return - the corresponding DTO
     */
    public AccommodationDto convertToDto(Accommodation accommodation){
        AccommodationDto accommodationDto = new AccommodationDto();
        accommodationDto.setName(accommodation.getName());
        accommodationDto.setPricePerMonth(accommodation.getPricePerMonth());
        accommodationDto.setRentType(accommodation.getRentType());
        accommodationDto.setDistanceFromUniversity(accommodation.getDistanceFromUniversity());
        return accommodationDto;
    }
}
