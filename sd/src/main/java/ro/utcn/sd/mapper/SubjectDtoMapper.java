package ro.utcn.sd.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.subject.SubjectDto;
import ro.utcn.sd.entity.Subject;
import ro.utcn.sd.service.SubjectService;

@Component
public class SubjectDtoMapper {
    @Autowired
    private SubjectService subjectService;

    public SubjectDto convertToDto(Subject subject) {
        SubjectDto subjectDto = new SubjectDto();
        subjectDto.setCredits(subject.getCredits());
        subjectDto.setSubjectCode(subject.getSubjectCode());
        subjectDto.setSubjectName(subject.getSubjectName());
        subjectDto.setTerm(subject.getTerm());
        return subjectDto;
    }

    public Subject convertToEntity(SubjectDto subjectDto){
        Subject subject = new Subject();
        subject.setCredits(subjectDto.getCredits());
        subject.setSubjectCode(subjectDto.getSubjectCode());
        subject.setTerm(subjectDto.getTerm());
        subject.setSubjectName(subjectDto.getSubjectName());
        return subject;
    }
}
