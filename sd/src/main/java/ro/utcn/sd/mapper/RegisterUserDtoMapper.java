package ro.utcn.sd.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.user.RegisterUserDto;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.StudentService;

import java.util.UUID;

import static java.util.Objects.isNull;

@Component
public class RegisterUserDtoMapper {

    @Autowired
    private StudentService studentService;

    public ApplicationUser convertToEntity(RegisterUserDto userDto) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        ApplicationUser user = new ApplicationUser
                .ApplicationUserBuilder(userDto.getFirstName(), userDto.getLastName())
                .email(userDto.getEmail())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .build();
        return user;
    }

    public Student convertToStudent(RegisterUserDto user, ApplicationUser applicationUser){
        Student student = new Student();
        student.setUser(applicationUser);
        student.setId(UUID.randomUUID().toString());
        student.setAverageGrade(user.getAverageGrade());
        student.setDateOfBirth(user.getDateOfBirth());
        student.setYearOfStudy(user.getYearOfStudy());
        student.setAddress(studentService.getStudentById("1").getAddress());
        return student;
    }
}
