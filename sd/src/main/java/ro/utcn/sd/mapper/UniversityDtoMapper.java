package ro.utcn.sd.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.university.UniversityDto;
import ro.utcn.sd.entity.University;

import java.util.UUID;

import static java.util.Objects.isNull;

@Component
public class UniversityDtoMapper {
    @Autowired
    private TeacherDtoMapper teacherDtoMapper;

    public UniversityDto convertToDto(University university) {
        UniversityDto universityDto = new UniversityDto();
        universityDto.setId(university.getId());
        universityDto.setName(university.getName());
        universityDto.setNumberOfApplicants(university.getMaximumAdmittedStudents());
        universityDto.setTeacher(teacherDtoMapper.convertToDto(university.getTeacher()));
        return universityDto;
    }

    public University convertToEntity(UniversityDto universityDto) {
        University university = new University();
        university.setTeacher(teacherDtoMapper.convertToEntity(universityDto.getTeacher()));
        university.setName(universityDto.getName());
        university.setMaximumAdmittedStudents(universityDto.getNumberOfApplicants());
        if (!isNull(universityDto.getId()))
            university.setId(universityDto.getId());
        else
            university.setId(UUID.randomUUID().toString());
        return university;
    }
}
