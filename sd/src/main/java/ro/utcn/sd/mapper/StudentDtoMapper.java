package ro.utcn.sd.mapper;

import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.student.StudentDto;
import ro.utcn.sd.entity.Student;

import java.util.UUID;

@Component
public class StudentDtoMapper {

    public StudentDto convertToDto(Student student) {
        StudentDto studentDto = new StudentDto();
        studentDto.setFirstName(student.getUser().getFirstName());
        studentDto.setLastName(student.getUser().getLastName());
        studentDto.setYear(student.getYearOfStudy());
        studentDto.setAverageGrade(student.getAverageGrade());
        return studentDto;
    }

    public Student convertToEntity(StudentDto studentDto) {
        Student student = new Student();
        student.setId(UUID.randomUUID().toString());
        return student;
    }
}
