package ro.utcn.sd.mapper;

import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.user.ApplicationUserDto;
import ro.utcn.sd.entity.ApplicationUser;

@Component
public class ApplicationUserDtoMapper {

    public ApplicationUser convertToEntity(ApplicationUserDto applicationUserDto){
        ApplicationUser applicationUser = new ApplicationUser();
        return applicationUser;
    }

    public ApplicationUserDto convertToDto(ApplicationUser applicationUser){
        ApplicationUserDto applicationUserDto = new ApplicationUserDto();
        return applicationUserDto;
    }
}
