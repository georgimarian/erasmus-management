package ro.utcn.sd.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.mobility.MobilityDto;
import ro.utcn.sd.dto.mobility.MobilityListDto;
import ro.utcn.sd.entity.Mobility;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MobilityListDtoMapper {
    @Autowired
    private MobilityDtoMapper mobilityDtoMapper;

    public MobilityListDto convertToDto(List<Mobility> mobilities) {
        System.out.println(mobilities);
        List<MobilityDto> mobilityDtos = mobilities.stream().map(mobility -> mobilityDtoMapper.convertToDto(mobility)).collect(Collectors.toList());
        MobilityListDto mobilityListDto = new MobilityListDto();
        mobilityListDto.setMobilityDtoList(mobilityDtos);
        return mobilityListDto;
    }
}
