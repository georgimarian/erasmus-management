package ro.utcn.sd.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.mobility.MobilityDto;
import ro.utcn.sd.dto.subject.SubjectDto;
import ro.utcn.sd.dto.subject.SubjectListDto;
import ro.utcn.sd.entity.Mobility;
import ro.utcn.sd.entity.Subject;
import ro.utcn.sd.service.MobilityService;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Component
public class MobilityDtoMapper {
    @Autowired
    private AccommodationDtoMapper accommodationDtoMapper;

    @Autowired
    private ApplicationDtoMapper applicationDtoMapper;

    @Autowired
    private SubjectDtoMapper subjectDtoMapper;

    @Autowired
    private MobilityService mobilityService;

    public MobilityDto convertToDto(Mobility mobility) {
        MobilityDto mobilityDto = new MobilityDto();
        mobilityDto.setAccommodation(accommodationDtoMapper.convertToDto(mobility.getAccommodation()));
        mobilityDto.setApplication(applicationDtoMapper.convertToDto(mobility.getApplication()));
        Set<Subject> subjects = mobility.getSubjects();
        List<SubjectDto> subjectDtos = subjects.stream().map(s -> subjectDtoMapper.convertToDto(s)).collect(Collectors.toList());
        SubjectListDto subjectListDto = new SubjectListDto();
        subjectListDto.setSubjects(subjectDtos);
        mobilityDto.setSubjects(subjectListDto);
        return mobilityDto;
    }

    public Mobility convertToEntity(MobilityDto mobilityDto, String id) {
        Mobility mobility = mobilityService.getMobilityById(id);
        if(isNull(mobility))
            mobility.setId(UUID.randomUUID().toString());
        mobility.setAccommodation(accommodationDtoMapper.convertToEntity(mobilityDto.getAccommodation()));
        mobility.setApplication(applicationDtoMapper.convertToEntity(mobilityDto.getApplication()));
        Set<Subject> subjects = mobilityDto.getSubjects().getSubjects().stream().map(subject -> subjectDtoMapper.convertToEntity(subject)).collect(Collectors.toSet());
        mobility.setSubjects(subjects);
        return mobility;
    }
}
