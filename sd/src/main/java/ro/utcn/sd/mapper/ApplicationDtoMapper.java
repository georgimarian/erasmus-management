package ro.utcn.sd.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.utcn.sd.dto.application.ApplicationDto;
import ro.utcn.sd.dto.application.NewApplicationDto;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Document;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.entity.enums.Status;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.DocumentService;
import ro.utcn.sd.service.StudentService;

import java.util.Date;
import java.util.UUID;

/**
 * Application to DTO mapper
 */
@Component
public class ApplicationDtoMapper {

    @Autowired
    private UniversityDtoMapper universityDtoMapper;

    @Autowired
    private StudentDtoMapper studentDtoMapper;

    @Autowired
    private StudentService studentService;

    @Autowired
    private ApplicationUserService userService;

    @Autowired
    private DocumentService documentService;


    /**
     * Convert Application to DTO
     *
     * @param application - Application to be converted
     * @return - conversion result
     */
    public ApplicationDto convertToDto(Application application) {
        ApplicationDto applicationDto = new ApplicationDto();
        applicationDto.setAddress(application.getAddress());
        applicationDto.setCreationDate(application.getCreationDate());
        applicationDto.setDocument(application.getDocument());
        applicationDto.setId(application.getId());
        applicationDto.setStatus(application.getStatus());
        applicationDto.setStudent(studentDtoMapper.convertToDto(application.getStudent()));
        applicationDto.setUniversity(universityDtoMapper.convertToDto(application.getUniversity()));
        return applicationDto;
    }

    /**
     * Convert ApplicationDto to Application
     *
     * @param applicationDto - DTO to be converted
     * @return - conversion result
     */
    public Application convertToEntity(ApplicationDto applicationDto) {
        Application application = new Application();
        application.setId(UUID.randomUUID().toString());
        application.setAddress(applicationDto.getAddress());
        application.setCreationDate(applicationDto.getCreationDate());
        application.setDocument(applicationDto.getDocument());
        application.setStatus(applicationDto.getStatus());
        application.setStudent(studentDtoMapper.convertToEntity(applicationDto.getStudent()));
        application.setUniversity(universityDtoMapper.convertToEntity(applicationDto.getUniversity()));
        return application;
    }

    public Application convertNewToEntity(NewApplicationDto applicationDto) {
        Student student = studentService.getStudentByUser(userService.findByEmail(applicationDto.getStudent()));
        Application application = new Application();
        application.setId(UUID.randomUUID().toString());
        application.setAddress(student.getAddress());
        Document document = documentService.getDocumentById("1");
        application.setDocument(document);
        application.setStudent(student);
        application.setCreationDate(new Date());
        application.setStatus(Status.PENDING);
        application.setUniversity(universityDtoMapper.convertToEntity(applicationDto.getUniversity()));
        return application;
    }
}
