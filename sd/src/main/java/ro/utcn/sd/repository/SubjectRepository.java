package ro.utcn.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.sd.entity.Subject;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, String> {
}
