package ro.utcn.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.sd.entity.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, String> {
    Document getDocumentById(String id);
}
