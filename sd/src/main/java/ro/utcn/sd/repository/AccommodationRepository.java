package ro.utcn.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.sd.entity.Accommodation;
import ro.utcn.sd.entity.University;

import java.util.List;

@Repository
public interface AccommodationRepository extends JpaRepository<Accommodation, String> {
    List<Accommodation> getAccommodationsByUniversity(University university);

    List<Accommodation> getAccommodationById(String id);
}
