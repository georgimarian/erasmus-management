package ro.utcn.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.entity.University;

import java.util.List;

@Repository
public interface StudentRepository extends JpaRepository<Student, String> {
    Student getStudentById(String id);

    Student getStudentByUser(ApplicationUser user);

}
