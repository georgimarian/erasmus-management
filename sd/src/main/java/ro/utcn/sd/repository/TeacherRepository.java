package ro.utcn.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.sd.entity.Teacher;
import ro.utcn.sd.entity.ApplicationUser;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, String> {
    Teacher findTeacherByUser(ApplicationUser u);

    Teacher findTeacherById(String id);

    Teacher findTeacherByUserContaining(String name);
}
