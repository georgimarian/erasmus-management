package ro.utcn.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.entity.Mobility;

@Repository
public interface MobilityRepository extends JpaRepository<Mobility, String> {
    Mobility findMobilityByApplication(Application application);

    Mobility findMobilityById(String id);
}
