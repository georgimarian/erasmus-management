package ro.utcn.sd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.utcn.sd.entity.Teacher;
import ro.utcn.sd.entity.University;

import java.util.Set;

@Repository
public interface UniversityRepository extends JpaRepository<University, String> {
    Set<University> findUniversitiesByTeacher(Teacher teacher);

    University findUniversityById(String id);

}
