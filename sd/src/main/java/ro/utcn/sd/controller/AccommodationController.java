package ro.utcn.sd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.utcn.sd.dto.accommodation.AccommodationDto;
import ro.utcn.sd.dto.accommodation.AccommodationListDto;
import ro.utcn.sd.dto.response.ResponseDtoFactory;
import ro.utcn.sd.entity.Accommodation;
import ro.utcn.sd.entity.University;
import ro.utcn.sd.mapper.AccommodationDtoMapper;
import ro.utcn.sd.service.AccommodationService;
import ro.utcn.sd.service.UniversityService;
import ro.utcn.sd.util.constants.ResponseStrings;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/accommodation")
public class AccommodationController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private AccommodationService accommodationService;

    @Autowired
    private UniversityService universityService;

    @Autowired
    private AccommodationDtoMapper accommodationDtoMapper;

    /**
     * Get Accommodations by University
     *
     * @param id
     * @return
     */
    @GetMapping("/university/{id}")
    public ResponseEntity getByUniversityId(@PathVariable String id) {
        try {
            University university = universityService.getUniversityById(id);
            List<Accommodation> accommodations = accommodationService.getAccommodationsByUniversity(university);
            List<AccommodationDto> accommodationDtos = accommodations.stream().map(a ->
                    accommodationDtoMapper.convertToDto(a)).collect(Collectors.toList());
            AccommodationListDto accommodationListDto = new AccommodationListDto(accommodationDtos);
            log.info(ResponseStrings.GET_ACCOMMODATION_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_ACCOMMODATION_SUCCESS, accommodationListDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_ACCOMMODATION_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_ACCOMMODATION_ERROR);
        }
    }

    /**
     * Add Application
     *
     * @param accommodationDto - data to be added
     * @return - ResponseEntity
     */

    @PostMapping("/university/{id}/create")
    public ResponseEntity create(@PathVariable String id, @RequestBody AccommodationDto accommodationDto) {
        try {
            University university = universityService.getUniversityById(id);
            Accommodation accommodation = accommodationDtoMapper.convertToEntity(accommodationDto);
            accommodation.setUniversity(university);
            accommodation.setId(UUID.randomUUID().toString());
            accommodationService.add(accommodation);
            log.info(ResponseStrings.ADD_ACCOMMODATION_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.ADD_ACCOMMODATION_SUCCESS, accommodationDto);
        } catch (Exception e) {
            System.out.println(e.toString());
            log.error(ResponseStrings.ADD_ACCOMMODATION_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.ADD_ACCOMMODATION_ERROR);
        }
    }

}
