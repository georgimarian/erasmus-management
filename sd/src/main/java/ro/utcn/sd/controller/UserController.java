package ro.utcn.sd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ro.utcn.sd.dto.ResetDto;
import ro.utcn.sd.dto.TokenDto;
import ro.utcn.sd.dto.response.ResponseDtoFactory;
import ro.utcn.sd.dto.user.LoginUserDto;
import ro.utcn.sd.dto.user.RegisterUserDto;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.mapper.RegisterUserDtoMapper;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.StudentService;
import ro.utcn.sd.util.EmailStrings;
import ro.utcn.sd.util.MailService;
import ro.utcn.sd.util.constants.ResponseStrings;
import ro.utcn.sd.util.constants.ValidationStrings;

import static java.util.Objects.isNull;

@CrossOrigin
@RestController
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private ApplicationUserService userService;

    @Autowired
    private RegisterUserDtoMapper registerUserDtoMapper;

    @Autowired
    private StudentService studentService;

    @Autowired
    private MailService mailService;

    /**
     * Method performing user registration (student account creation)
     *
     * @param registerUserDto user dto to be registered
     * @return
     */
    @PostMapping("/register")
    public ResponseEntity signUp(@RequestBody RegisterUserDto registerUserDto) {
        try {
            System.out.println(registerUserDto);
            ApplicationUser applicationUser = registerUserDtoMapper.convertToEntity(registerUserDto);
            System.out.println(applicationUser);

            if (!isNull(userService.findByEmail(registerUserDto.getEmail()))) {
                log.error(ValidationStrings.CANNOT_ADD_EMAIL);
                return ResponseDtoFactory.createErrorMessage(ValidationStrings.EMAIL_EXISTS);
            }
            userService.save(applicationUser);
            Student student = registerUserDtoMapper.convertToStudent(registerUserDto, applicationUser);
            studentService.save(student);
            mailService.sendMail(registerUserDto.getEmail(), EmailStrings.WELCOME, EmailStrings.REGISTER_SUCCESS);
            log.info(ResponseStrings.ADD_STUDENT_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.REGISTER_SUCCESS, registerUserDto);
        } catch (Exception e) {
            System.out.println(e.toString());
            log.error(ResponseStrings.REGISTER_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.REGISTER_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Method that deals with user log-in
     *
     * @param loginUserDto user received from request
     * @return
     */
    @PostMapping(path = "/login", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity login(@RequestBody LoginUserDto loginUserDto) {
        try {
            ApplicationUser user = userService.findByEmailAndPassword(loginUserDto.getEmail(), loginUserDto.getPassword());
            if (isNull(user)) {
                log.error(ValidationStrings.INVALID_CREDENTIALS_ATTEMPT);
                return ResponseDtoFactory.createErrorMessage(ValidationStrings.INVALID_CREDENTIALS);
            }
            TokenDto tokenDto = new TokenDto();
            tokenDto.setMessage(userService.generateToken(user));
            log.info(ValidationStrings.SUCCESS_CREATE_TOKEN);
            return ResponseDtoFactory.createSuccessMessage(ValidationStrings.SUCCESS_CREATE_TOKEN, tokenDto);
        } catch (Exception e) {
            log.error(ValidationStrings.ERROR_LOGIN);
            return ResponseDtoFactory.createErrorMessage(ValidationStrings.ERROR_LOGIN);
        }
    }

    @PostMapping("/reset")
    public ResponseEntity resetPassword(@RequestBody LoginUserDto user) {
        try {
            ApplicationUser applicationUser = userService.findByEmail(user.getEmail());
            if (isNull(applicationUser)) {
                log.error(ValidationStrings.EMAIL_NOT_EXISTS);
                return ResponseDtoFactory.createErrorMessage(ValidationStrings.EMAIL_NOT_EXISTS);
            }
            String newPassword = userService.resetPassword(applicationUser);
            applicationUser.setPassword(newPassword);
            userService.save(applicationUser);
            mailService.sendMail(user.getEmail(), EmailStrings.RESET_PASSWORD, ResponseStrings.RESET_BODY + newPassword);
            log.info(ResponseStrings.CHANGE_PASSWORD_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.CHANGE_PASSWORD_SUCCESS, user);
        } catch (Exception e) {
            log.error(ResponseStrings.CHANGE_PASSWORD_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.CHANGE_PASSWORD_ERROR);
        }
    }

    @PostMapping("/change-password")
    public ResponseEntity changePassword(@RequestBody ResetDto user) {
        try {
            ApplicationUser applicationUser = userService.findByEmail(user.getEmail());
            if (isNull(applicationUser)) {
                log.error(ValidationStrings.EMAIL_NOT_EXISTS);
                return ResponseDtoFactory.createErrorMessage(ValidationStrings.EMAIL_NOT_EXISTS);
            }
            if (user.getCode().equals(applicationUser.getPassword()) && user.getPassword().equals(user.getPassword2())) {
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                applicationUser.setPassword(passwordEncoder.encode(user.getPassword()));
                userService.save(applicationUser);
                mailService.sendMail(user.getEmail(), EmailStrings.RESET_PASSWORD, "Password changed");
                log.info(ResponseStrings.CHANGE_PASSWORD_SUCCESS);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.CHANGE_PASSWORD_SUCCESS, user);
            }
            log.error(ResponseStrings.CHANGE_PASSWORD_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.CHANGE_PASSWORD_ERROR);

        } catch (Exception e) {
            log.error(ResponseStrings.CHANGE_PASSWORD_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.CHANGE_PASSWORD_ERROR);
        }
    }
}
