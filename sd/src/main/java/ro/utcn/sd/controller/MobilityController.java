package ro.utcn.sd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.utcn.sd.dto.mobility.MobilityDto;
import ro.utcn.sd.dto.mobility.MobilityListDto;
import ro.utcn.sd.dto.response.ResponseDtoFactory;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Mobility;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.mapper.MobilityDtoMapper;
import ro.utcn.sd.mapper.MobilityListDtoMapper;
import ro.utcn.sd.service.ApplicationService;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.MobilityService;
import ro.utcn.sd.service.StudentService;
import ro.utcn.sd.util.constants.ResponseStrings;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/mobility")
public class MobilityController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private MobilityService mobilityService;

    @Autowired
    private MobilityDtoMapper mobilityDtoMapper;

    @Autowired
    private ApplicationUserService userService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private MobilityListDtoMapper mobilityListDtoMapper;

    /**
     * GET all Mobilities
     *
     * @return - ResponseEntity containing MobilityDto list
     */
    @GetMapping("/all")
    public ResponseEntity getAll() {
        try {
            MobilityListDto mobilityListDto = mobilityListDtoMapper.convertToDto(mobilityService.getAllMobilities());
            log.info(ResponseStrings.GET_MOBILITIES_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_MOBILITIES_SUCCESS, mobilityListDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_MOBILITIES_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_MOBILITIES_ERROR);
        }
    }

    /**
     * Get mobility by Student E-mail
     *
     * @param email - e-mail to be found
     * @return - ResponseEntity with the Mobility
     */
    @GetMapping("/student/{email}")
    public ResponseEntity getMobilitiesByStudent(@PathVariable String email) {
        try {
            ApplicationUser user = userService.findByEmail(email);
            Student student = studentService.getStudentByUser(user);
            List<Application> applications = applicationService.getApplicationsByStudent(student);
            List<Mobility> mobilities = applications.stream().map(application -> mobilityService.getMobilityByApplication(application)).collect(Collectors.toList());
            mobilities.removeAll(Collections.singleton(null));
            MobilityListDto mobilityListDto = mobilityListDtoMapper.convertToDto(mobilities);
            log.info(ResponseStrings.GET_MOBILITIES_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_MOBILITIES_SUCCESS, mobilityListDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_MOBILITIES_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_MOBILITIES_ERROR);
        }
    }

    /**
     * UPDATE Mobility
     *
     * @param id          - UUID of the Mobility to be updated
     * @param mobilityDto - the data to be changed
     * @return - ResponseEntity
     */
    @PutMapping("/edit/{id}")
    public ResponseEntity updateById(@PathVariable String id, @RequestBody MobilityDto mobilityDto) {
        try {
            Mobility mobility = mobilityDtoMapper.convertToEntity(mobilityDto, id);
            mobilityService.editMobility(mobility);
            MobilityDto editedMobilityDto = mobilityDtoMapper.convertToDto(mobility);
            log.info(ResponseStrings.EDIT_MOBILITY_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.EDIT_MOBILITY_SUCCESS, editedMobilityDto);
        } catch (Exception e) {
            log.error(ResponseStrings.EDIT_MOBILITY_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.EDIT_MOBILITY_ERROR);
        }
    }
}
