package ro.utcn.sd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.utcn.sd.dto.application.ApplicationDto;
import ro.utcn.sd.dto.application.ApplicationListDto;
import ro.utcn.sd.dto.application.NewApplicationDto;
import ro.utcn.sd.dto.response.ResponseDtoFactory;
import ro.utcn.sd.entity.Application;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.entity.enums.Status;
import ro.utcn.sd.mapper.ApplicationDtoMapper;
import ro.utcn.sd.service.ApplicationService;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.StudentService;
import ro.utcn.sd.util.EmailStrings;
import ro.utcn.sd.util.MailService;
import ro.utcn.sd.util.constants.ResponseStrings;

import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/application")
public class ApplicationController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private ApplicationDtoMapper applicationDtoMapper;

    @Autowired
    private ApplicationUserService userService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private MailService mailService;

    /**
     * GET List of all Application
     *
     * @return - list of all Application
     */
    @GetMapping("/all")
    public ResponseEntity getAll() {
        try {
            List<Application> applications = applicationService.getAllApplications();
            List<ApplicationDto> applicationDtos = applications.stream().map(a -> applicationDtoMapper.convertToDto(a)).collect(Collectors.toList());
            ApplicationListDto applicationListDto = new ApplicationListDto(applicationDtos);
            log.info(ResponseStrings.GET_APPLICATIONS_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_APPLICATIONS_SUCCESS, applicationListDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_APPLICATIONS_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_APPLICATIONS_ERROR);
        }
    }

    /**
     * GET Application by UUID
     *
     * @param id - UUID to be found
     * @return - ResponseEntity
     */
    @GetMapping("/{id}")
    public ResponseEntity getApplicationById(@PathVariable String id) {
        try {
            Application applcation = applicationService.getApplicationById(id);
            ApplicationDto applicationDto = applicationDtoMapper.convertToDto(applcation);
            log.info(ResponseStrings.GET_APPLICATION_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_APPLICATION_SUCCESS, applicationDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_APPLICATION_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_APPLICATION_ERROR);
        }
    }

    /**
     * Get Application by Student
     *
     * @param email - e-mail of Student
     * @return - ResponseEntity
     */
    @GetMapping("/student/{email}")
    public ResponseEntity getApplicationByStudentEmail(@PathVariable String email) {
        try {
            ApplicationUser user = userService.findByEmail(email);
            Student student = studentService.getStudentByUser(user);
            List<Application> applications = applicationService.getApplicationsByStudent(student);
            List<ApplicationDto> applicationDtos = applications.stream().map(a -> applicationDtoMapper.convertToDto(a)).collect(Collectors.toList());
            ApplicationListDto applicationDto = new ApplicationListDto(applicationDtos);
            log.info(ResponseStrings.GET_APPLICATION_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_APPLICATION_SUCCESS, applicationDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_APPLICATION_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_APPLICATION_ERROR);
        }
    }

    /**
     * Add Application
     *
     * @param applicationDto - data to be added
     * @return - ResponseEntity
     */

    @PostMapping("/create")
    public ResponseEntity create(@RequestBody NewApplicationDto applicationDto) {
        try {
            Application application = applicationDtoMapper.convertNewToEntity(applicationDto);
            System.out.println(application.toString());
            applicationService.addApplication(application);
            mailService.sendApplication(application.getStudent().getUser().getEmail(), EmailStrings.NEW_APPLICATION, EmailStrings.NEW_APPLICATION_BODY, application);
            log.info(ResponseStrings.ADD_APPLICATION_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.ADD_APPLICATION_ERROR, applicationDto);
        } catch (Exception e) {
            Application application = applicationDtoMapper.convertNewToEntity(applicationDto);
            applicationService.deleteApplication(application);
            System.out.println(e.toString());
            log.error(ResponseStrings.ADD_APPLICATION_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.ADD_APPLICATION_ERROR);
        }
    }

    /**
     * Update Application
     *
     * @param id             - UUID of application to be updated
     * @param applicationDto - data to be changed
     * @return - ResponseEntity
     */

    @PutMapping("/edit/{id}")
    public ResponseEntity updateById(@PathVariable String id, @RequestBody ApplicationDto applicationDto) {
        try {
            Application application = applicationService.getApplicationById(id);
            application.setStatus(applicationDto.getStatus());
            applicationService.editApplication(application);
            if(applicationDto.getStatus().equals(Status.DENIED)){
                mailService.sendApplication(application.getStudent().getUser().getEmail(), EmailStrings.REJECTED_APPLICATION, EmailStrings.APPLICATION_DENIED,application);
            }
            log.info(ResponseStrings.EDIT_APPLICATION_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.EDIT_APPLICATION_SUCCESS, applicationDto);
        } catch (Exception e) {
            log.error(ResponseStrings.EDIT_APPLICATION_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.EDIT_APPLICATION_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * DELETE an Application
     *
     * @param id - UUID of Application to be deleted
     * @return - ResponseEntity
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteById(@PathVariable String id) {
        try {
            Application application = applicationService.getApplicationById(id);
            if (!applicationService.deleteApplication(application))
                return ResponseDtoFactory.createErrorMessage(ResponseStrings.CANNOT_DELETE_ONGOING, HttpStatus.BAD_REQUEST);
            ApplicationDto applicationDto = applicationDtoMapper.convertToDto(application);
            log.info(ResponseStrings.DELETE_APPLICATION_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.DELETE_APPLICATION_SUCCESS, applicationDto);
        } catch (Exception e) {
            log.error(ResponseStrings.DELETE_APPLICATION_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.DELETE_APPLICATION_ERROR);
        }
    }
}
