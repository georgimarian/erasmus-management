package ro.utcn.sd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.utcn.sd.dto.MessageDto;
import ro.utcn.sd.dto.response.ResponseDtoFactory;
import ro.utcn.sd.dto.teacher.TeacherDto;
import ro.utcn.sd.dto.teacher.TeacherListDto;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Teacher;
import ro.utcn.sd.mapper.TeacherDtoMapper;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.TeacherService;
import ro.utcn.sd.util.MailService;
import ro.utcn.sd.util.constants.ResponseStrings;
import ro.utcn.sd.util.constants.Role;
import ro.utcn.sd.util.constants.ValidationStrings;

import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/teacher")
public class TeacherController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TeacherDtoMapper teacherDtoMapper;

    @Autowired
    private ApplicationUserService userService;

    @Autowired
    private MailService mailService;

    /**
     * GET Method that gets all Teacher entities from the database
     *
     * @param token - authorization token
     * @return
     */
    @GetMapping("/all")
    public ResponseEntity getAll(@RequestHeader("Authorization") String token) {
        try {
            ApplicationUser user = userService.getTokenBearer(token.substring((7)));
            if (userService.findUserRole(user).equals(Role.ADMIN.toString()) || userService.findUserRole(user).equals(Role.STUDENT.toString())) {
                List<Teacher> teachers = teacherService.getAllTeachers();
                List<TeacherDto> teacherDtos = teachers.stream().map(TeacherDtoMapper::convertToDto).collect(Collectors.toList());
                TeacherListDto teacherListDto = new TeacherListDto();
                teacherListDto.setTeacherDtoList(teacherDtos);
                log.info(ResponseStrings.GET_TEACHERS_SUCCESS);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_TEACHERS_SUCCESS, teacherListDto);
            }
            return ResponseDtoFactory.createUnauthorizedMessage(ValidationStrings.INVALID_TOKEN);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_TEACHERS_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_TEACHERS_ERROR);
        }
    }

    /**
     * GET method to get a Teacher entity by its databse UUID
     *
     * @param id UUID
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity getTeacherById(@PathVariable String id) {
        try {
            Teacher teacher = teacherService.findById(id);
            TeacherDto teacherDto = teacherDtoMapper.convertToDto(teacher);
            log.info(ResponseStrings.GET_TEACHER_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_TEACHER_SUCCESS, teacherDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_TEACHER_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_TEACHER_ERROR);
        }
    }

    @PostMapping("/contact/{id}")
    public ResponseEntity contactTeacher(@PathVariable String id, @RequestBody MessageDto messageDto, @RequestHeader("Authorization") String token) {
        try {
            ApplicationUser user = userService.getTokenBearer(token.substring((7)));
            if (userService.findUserRole(user).equals(Role.STUDENT.toString())) {
                Teacher teacher = teacherService.findById(id);
                mailService.sendMail(teacher.getUser().getEmail(), messageDto.getSubject(), messageDto.getMessage());
                log.info(ResponseStrings.MESSAGE_SENT);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.MESSAGE_SENT, messageDto);
            }
            log.warn(ValidationStrings.INVALID_TOKEN);
            return ResponseDtoFactory.createUnauthorizedMessage(ValidationStrings.INVALID_TOKEN);
        } catch (Exception e) {
            log.error(ResponseStrings.MESSAGE_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.MESSAGE_ERROR);
        }
    }

    /**
     * DELETE method to remove a Teacher entity from the database
     *
     * @param id    UUID of teacher to be deleted (in the database)
     * @param token authorization token
     * @return
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteById(@PathVariable String id, @RequestHeader("Authorization") String token) {
        try {
            ApplicationUser user = userService.getTokenBearer(token.substring((7)));
            if (userService.findUserRole(user).equals(Role.ADMIN.toString())) {
                Teacher teacher = teacherService.findById(id);
                TeacherDto teacherDto = teacherDtoMapper.convertToDto(teacher);
                teacherService.deleteTeacher(teacher);
                log.info(ResponseStrings.DELETE_TEACHER_SUCCESS);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.DELETE_TEACHER_SUCCESS, teacherDto);
            }
            log.warn(ValidationStrings.INVALID_TOKEN);
            return ResponseDtoFactory.createUnauthorizedMessage(ValidationStrings.INVALID_TOKEN);
        } catch (Exception e) {
            log.error(ResponseStrings.DELETE_TEACHER_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.DELETE_TEACHER_ERROR);
        }
    }
}
