package ro.utcn.sd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.utcn.sd.dto.response.ResponseDtoFactory;
import ro.utcn.sd.dto.student.StudentDto;
import ro.utcn.sd.dto.student.StudentListDto;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.Student;
import ro.utcn.sd.mapper.StudentDtoMapper;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.StudentService;
import ro.utcn.sd.util.constants.ResponseStrings;

import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/student")
public class StudentController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private StudentService studentService;

    @Autowired
    private StudentDtoMapper studentDtoMapper;

    @Autowired
    private ApplicationUserService applicationUserService;

    /**
     * GET Student by UUID
     *
     * @param id - the UUID to be found
     * @return ResponseEntity containing the StudentDto
     */
    @GetMapping("/{id}")
    public ResponseEntity getStudentById(@PathVariable String id) {
        try {
            Student student = studentService.getStudentById(id);
            StudentDto studentDto = studentDtoMapper.convertToDto(student);
            log.info(ResponseStrings.GET_STUDENT_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_STUDENT_SUCCESS, studentDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_STUDENT_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_STUDENT_ERROR);
        }
    }

    /**
     * GET student by e-mail
     *
     * @param email - e-mail to be found
     * @return - ResponseEntity containing StudentDto
     */
    @GetMapping("/email/{email}")
    public ResponseEntity getStudentByEmail(@PathVariable String email) {
        try {
            ApplicationUser user = applicationUserService.findByEmail(email);
            Student student = studentService.getStudentByUser(user);
            StudentDto studentDto = studentDtoMapper.convertToDto(student);
            log.info(ResponseStrings.GET_STUDENT_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_STUDENT_SUCCESS, studentDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_STUDENT_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_STUDENT_ERROR);
        }
    }

    /**
     * GET all Students
     *
     * @return - ResponseEntity containing StudentListDto
     */
    @GetMapping("/all")
    public ResponseEntity getAll() {
        try {
            List<Student> students = studentService.getAllStudents();
            List<StudentDto> studentDtos = students.stream().map(student -> studentDtoMapper.convertToDto(student)).collect(Collectors.toList());
            StudentListDto studentListDto = new StudentListDto();
            studentListDto.setStudents(studentDtos);
            log.info(ResponseStrings.GET_STUDENTS_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_STUDENTS_SUCCESS, studentListDto);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_STUDENTS_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_STUDENTS_ERROR);
        }
    }

    /**
     * DELETE a Student
     *
     * @param id - UUID of the student to be deleted
     * @return
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteById(@PathVariable String id) {
        try {
            Student student = studentService.getStudentById(id);
            StudentDto studentDto = studentDtoMapper.convertToDto(student);
            studentService.deleteStudent(student);
            log.info(ResponseStrings.DELETE_STUDENT_SUCCESS);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.DELETE_STUDENT_SUCCESS, studentDto);
        } catch (Exception e) {
            log.error(ResponseStrings.DELETE_STUDENT_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.DELETE_STUDENT_ERROR);
        }
    }
}
