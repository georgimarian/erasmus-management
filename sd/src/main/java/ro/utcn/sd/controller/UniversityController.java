package ro.utcn.sd.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.utcn.sd.dto.response.ResponseDtoFactory;
import ro.utcn.sd.dto.university.UniversityDto;
import ro.utcn.sd.dto.university.UniversityListDto;
import ro.utcn.sd.entity.ApplicationUser;
import ro.utcn.sd.entity.University;
import ro.utcn.sd.mapper.UniversityDtoMapper;
import ro.utcn.sd.service.ApplicationUserService;
import ro.utcn.sd.service.UniversityService;
import ro.utcn.sd.util.constants.ResponseStrings;
import ro.utcn.sd.util.constants.Role;
import ro.utcn.sd.util.constants.ValidationStrings;

import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/university")
public class UniversityController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UniversityService universityService;

    @Autowired
    private UniversityDtoMapper universityDtoMapper;

    @Autowired
    private ApplicationUserService userService;

    /**
     * GET university by UUID
     *
     * @param id - UUID to be found
     * @return - ResponseDTO containing University
     */
    @GetMapping("/{id}")
    public ResponseEntity getUniversityById(@PathVariable String id) {
        try {
            University university = universityService.getUniversityById(id);
            UniversityDto universityDto = universityDtoMapper.convertToDto(university);
            log.info("Got university by UUID" + id);
            return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_UNIVERSITY_SUCCESS, universityDto);
        } catch (Exception e) {
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_UNIVERSITY_ERROR);
        }
    }

    /**
     * Get all Universities
     *
     * @param token - JWT Access Token
     * @return
     */
    @GetMapping(path = "/all", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity getAll(@RequestHeader("Authorization") String token) {
        try {
            ApplicationUser user = userService.getTokenBearer(token.substring((7)));
            if (userService.findUserRole(user).equals(Role.ADMIN.toString()) || userService.findUserRole(user).equals(Role.STUDENT.toString())) {
                List<University> universities = universityService.getAllUniversities();
                List<UniversityDto> universityDtos = universities.stream().map(a -> universityDtoMapper.convertToDto(a)).collect(Collectors.toList());
                UniversityListDto universityListDto = new UniversityListDto();
                universityListDto.setUniversities(universityDtos);
                log.info(ResponseStrings.GET_UNIVERSITIES_SUCCESS);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.GET_UNIVERSITIES_SUCCESS, universityListDto);
            }
            log.warn(ValidationStrings.INVALID_TOKEN + " for getting universities!");
            return ResponseDtoFactory.createErrorMessage(ValidationStrings.INVALID_TOKEN + " for getting universities!", HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            log.error(ResponseStrings.GET_UNIVERSITIES_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.GET_UNIVERSITIES_ERROR);
        }
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity updateById(@PathVariable String id, @RequestBody UniversityDto universityDto, @RequestHeader("Authorization") String token) {
        try {
            ApplicationUser user = userService.getTokenBearer(token.substring((7)));
            if (userService.findUserRole(user).equals("admin")) {
                universityDto.setId(id);
                University university = universityDtoMapper.convertToEntity(universityDto);
                universityService.editUniversity(university);
                UniversityDto universityDto1 = universityDtoMapper.convertToDto(university);
                log.info(ResponseStrings.EDIT_UNIVERSITY_SUCCESS);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.EDIT_UNIVERSITY_SUCCESS, HttpStatus.OK, universityDto1);
            }
            log.warn(ValidationStrings.INVALID_TOKEN + " for editing universities!");
            return ResponseDtoFactory.createUnauthorizedMessage(ValidationStrings.INVALID_TOKEN + " for editing university!");
        } catch (Exception e) {
            log.error(ResponseStrings.EDIT_UNIVERSITY_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.EDIT_UNIVERSITY_ERROR);
        }
    }

    /**
     * ADD an university
     *
     * @param universityDto - data to be added
     * @param token         - JWT access token
     * @return - ResponseEntity
     */
    @PostMapping("/add")
    public ResponseEntity addUniversity(@RequestBody UniversityDto universityDto, @RequestHeader("Authorization") String token) {
        try {
            ApplicationUser user = userService.getTokenBearer(token.substring((7)));
            if (userService.findUserRole(user).equals("admin")) {
                University university = universityDtoMapper.convertToEntity(universityDto);
                universityService.saveUniversity(university);
                UniversityDto universityDto1 = universityDtoMapper.convertToDto(university);
                log.info(ResponseStrings.ADD_UNIVERSITY_SUCCESS + ": " + university);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.ADD_UNIVERSITY_SUCCESS, universityDto1);
            }
            log.warn(ValidationStrings.INVALID_TOKEN);
            return ResponseDtoFactory.createErrorMessage(ValidationStrings.INVALID_TOKEN + " for adding university!", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error(ResponseStrings.ADD_UNIVERSITY_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.ADD_UNIVERSITY_ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * DELETE University
     *
     * @param id    - UUID of University to be deleted
     * @param token - JWT authorization token
     * @return
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteById(@PathVariable String id, @RequestHeader("Authorization") String token) {
        try {
            ApplicationUser user = userService.getTokenBearer(token.substring((7)));
            if (userService.findUserRole(user).equals(Role.ADMIN.getName())) {
                University university = universityService.getUniversityById(id);
                UniversityDto universityDto = universityDtoMapper.convertToDto(university);
                if (!universityService.deleteUniversity(university)) {
                    log.warn(ValidationStrings.CANNOT_DELETE);
                    return ResponseDtoFactory.createErrorMessage(ResponseStrings.CANNOT_DELETE_HAS_MOBILITIES);
                }
                log.info(ResponseStrings.DELETE_UNIVERSITY_SUCCESS);
                return ResponseDtoFactory.createSuccessMessage(ResponseStrings.DELETE_UNIVERSITY_SUCCESS, universityDto);
            }
            log.warn(ValidationStrings.INVALID_TOKEN);
            return ResponseDtoFactory.createErrorMessage(ValidationStrings.INVALID_TOKEN + " for deleting university!", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error(ResponseStrings.DELETE_UNIVERSITY_ERROR);
            return ResponseDtoFactory.createErrorMessage(ResponseStrings.DELETE_UNIVERSITY_ERROR, HttpStatus.BAD_REQUEST);
        }
    }
}
