package ro.utcn.sd.dto.teacher;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.entity.ApplicationUser;

public class TeacherDto extends Dto {
    private String id;

    private String department;

    private ApplicationUser user;

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ApplicationUser getUser() {
        return user;
    }

    public void setUser(ApplicationUser user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TeacherDto{" +
                "id='" + id + '\'' +
                ", department='" + department + '\'' +
                ", user=" + user +
                '}';
    }
}
