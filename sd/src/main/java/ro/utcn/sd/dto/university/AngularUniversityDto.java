package ro.utcn.sd.dto.university;

public class AngularUniversityDto {
    private String teacher;
    private String name;
    private int numberOfApplicants;

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfApplicants() {
        return numberOfApplicants;
    }

    public void setNumberOfApplicants(int numberOfApplicants) {
        this.numberOfApplicants = numberOfApplicants;
    }

    @Override
    public String toString() {
        return "AngularUniversityDto{" +
                "teacherName='" + teacher + '\'' +
                ", name='" + name + '\'' +
                ", numberOfApplicants=" + numberOfApplicants +
                '}';
    }
}
