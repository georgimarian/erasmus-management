package ro.utcn.sd.dto.application;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.application.ApplicationDto;

import java.util.List;

public class ApplicationListDto extends Dto {
    private List<ApplicationDto> applicationDtoList;

    public ApplicationListDto(List<ApplicationDto> applications){
        this.applicationDtoList = applications;
    }

    public List<ApplicationDto> getApplicationDtoList() {
        return applicationDtoList;
    }

    public void setApplicationDtoList(List<ApplicationDto> applicationDtoList) {
        this.applicationDtoList = applicationDtoList;
    }
}
