package ro.utcn.sd.dto;

public class TokenDto extends Dto {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
