package ro.utcn.sd.dto.university;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.teacher.TeacherDto;

public class UniversityDto extends Dto {
    private String id;
    private String name;
    private int numberOfApplicants;
    private TeacherDto teacher;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfApplicants() {
        return numberOfApplicants;
    }

    public void setNumberOfApplicants(int numberOfApplicants) {
        this.numberOfApplicants = numberOfApplicants;
    }

    public TeacherDto getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDto teacher) {
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "UniversityDto{" +
                "name='" + name + '\'' +
                ", numberOfApplicants=" + numberOfApplicants +
                ", teacher=" + teacher +
                '}';
    }
}
