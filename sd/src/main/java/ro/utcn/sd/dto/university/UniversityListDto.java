package ro.utcn.sd.dto.university;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.university.UniversityDto;

import java.util.List;

public class UniversityListDto extends Dto {
    private List<UniversityDto> universities;

    public List<UniversityDto> getUniversities() {
        return universities;
    }

    public void setUniversities(List<UniversityDto> universities) {
        this.universities = universities;
    }
}
