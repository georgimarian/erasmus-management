package ro.utcn.sd.dto.accommodation;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.accommodation.AccommodationDto;

import java.util.List;

/**
 * Class having as attribute List of AccommodationDTO
 */
public class AccommodationListDto extends Dto {
    List<AccommodationDto> accommodationDtos;

    public List<AccommodationDto> getAccommodationDtos() {
        return accommodationDtos;
    }

    public void setAccommodationDtos(List<AccommodationDto> accommodationDtos) {
        this.accommodationDtos = accommodationDtos;
    }

    public AccommodationListDto(List<AccommodationDto> accommodationDtos){
        this.accommodationDtos = accommodationDtos;
    }

    @Override
    public String toString() {
        return "AccommodationListDto{" +
                "accommodationDtos=" + accommodationDtos +
                '}';
    }
}
