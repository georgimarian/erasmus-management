package ro.utcn.sd.dto.user;

import ro.utcn.sd.dto.Dto;

public class LoginUserDto extends Dto {
    private String email;

    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginUserDto{" +
                "username='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
