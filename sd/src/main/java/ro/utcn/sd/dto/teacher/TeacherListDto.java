package ro.utcn.sd.dto.teacher;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.teacher.TeacherDto;

import java.util.List;

public class TeacherListDto  extends Dto {
    List<TeacherDto> teacherDtoList;

    public List<TeacherDto> getTeacherDtoList() {
        return teacherDtoList;
    }

    public void setTeacherDtoList(List<TeacherDto> teacherDtoList) {
        this.teacherDtoList = teacherDtoList;
    }
}
