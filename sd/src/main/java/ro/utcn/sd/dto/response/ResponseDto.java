package ro.utcn.sd.dto.response;

import ro.utcn.sd.dto.Dto;

public class ResponseDto {

    private String message;
    private String severity;

    private Dto dto;

    public ResponseDto(){

    }
    public ResponseDto(String message, String severity) {
        this.message = message;
        this.severity = severity;
    }

    public ResponseDto(String message, String severity, Dto dto) {
        this.message = message;
        this.severity = severity;
        this.dto = dto;
    }

    public Dto getDto() {
        return dto;
    }

    public void setDto(Dto dto) {
        this.dto = dto;
    }

    public String getMessage() {
        return message;
    }

    public String getSeverity() {
        return severity;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    @Override
    public String toString() {
        return "ResponseDto{" +
                "message='" + message + '\'' +
                ", severity='" + severity + '\'' +
                '}';
    }
}
