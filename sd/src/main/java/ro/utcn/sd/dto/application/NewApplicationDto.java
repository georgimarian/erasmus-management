package ro.utcn.sd.dto.application;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.student.StudentDto;
import ro.utcn.sd.dto.university.UniversityDto;

public class NewApplicationDto extends Dto {
    private int year;
    private int semester;
    private UniversityDto universityDto;
    private String student;

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public UniversityDto getUniversity() {
        return universityDto;
    }

    public void setUniversity(UniversityDto universityDto) {
        this.universityDto = universityDto;
    }
}
