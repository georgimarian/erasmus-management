package ro.utcn.sd.dto.mobility;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.accommodation.AccommodationDto;
import ro.utcn.sd.dto.application.ApplicationDto;
import ro.utcn.sd.dto.subject.SubjectListDto;

public class MobilityDto extends Dto {
    private String id;
    private ApplicationDto application;
    private AccommodationDto accommodation;
    private SubjectListDto subjects;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ApplicationDto getApplication() {
        return application;
    }

    public void setApplication(ApplicationDto application) {
        this.application = application;
    }

    public AccommodationDto getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(AccommodationDto accommodation) {
        this.accommodation = accommodation;
    }

    public SubjectListDto getSubjects() {
        return subjects;
    }

    public void setSubjects(SubjectListDto subjects) {
        this.subjects = subjects;
    }
}
