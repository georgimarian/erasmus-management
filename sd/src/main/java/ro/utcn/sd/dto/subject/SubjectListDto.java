package ro.utcn.sd.dto.subject;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.subject.SubjectDto;

import java.util.List;

public class SubjectListDto extends Dto {
    List<SubjectDto> subjects;

    public List<SubjectDto> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectDto> subjects) {
        this.subjects = subjects;
    }
}
