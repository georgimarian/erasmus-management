package ro.utcn.sd.dto.student;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.student.StudentDto;

import java.util.List;

public class StudentListDto extends Dto {
    List<StudentDto> students;

    public List<StudentDto> getStudents() {
        return students;
    }

    public void setStudents(List<StudentDto> students) {
        this.students = students;
    }
}
