package ro.utcn.sd.dto.mobility;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.mobility.MobilityDto;

import java.util.List;

public class MobilityListDto extends Dto {
    List<MobilityDto> mobilityDtoList;

    public List<MobilityDto> getMobilityDtoList() {
        return mobilityDtoList;
    }

    public void setMobilityDtoList(List<MobilityDto> mobilityDtoList) {
        this.mobilityDtoList = mobilityDtoList;
    }

    @Override
    public String toString() {
        return "MobilityListDto{" +
                "mobilityDtoList=" + mobilityDtoList +
                '}';
    }
}
