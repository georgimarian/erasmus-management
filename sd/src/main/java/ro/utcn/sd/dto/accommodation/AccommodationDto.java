package ro.utcn.sd.dto.accommodation;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.entity.enums.RentType;

public class AccommodationDto extends Dto {
    private float distanceFromUniversity;

    private String name;

    private String pricePerMonth;

    private RentType rentType;

    public float getDistanceFromUniversity() {
        return distanceFromUniversity;
    }

    public void setDistanceFromUniversity(float distanceFromUniversity) {
        this.distanceFromUniversity = distanceFromUniversity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPricePerMonth() {
        return pricePerMonth;
    }

    public void setPricePerMonth(String pricePerMonth) {
        this.pricePerMonth = pricePerMonth;
    }

    public RentType getRentType() {
        return rentType;
    }

    public void setRentType(RentType rentType) {
        this.rentType = rentType;
    }

    @Override
    public String toString() {
        return "AccommodationDto{" +
                "distanceFromUniversity=" + distanceFromUniversity +
                ", name='" + name + '\'' +
                ", pricePerMonth=" + pricePerMonth +
                ", rentType=" + rentType +
                '}';
    }
}
