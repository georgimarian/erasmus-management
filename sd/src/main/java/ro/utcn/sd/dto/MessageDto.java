package ro.utcn.sd.dto;

public class MessageDto extends Dto {
    private String subject;

    private String message;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String body) {
        this.message = body;
    }

    @Override
    public String toString() {
        return "MessageDto{" +
                "subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
