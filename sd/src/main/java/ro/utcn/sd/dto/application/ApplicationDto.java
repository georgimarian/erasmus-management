package ro.utcn.sd.dto.application;

import ro.utcn.sd.dto.Dto;
import ro.utcn.sd.dto.student.StudentDto;
import ro.utcn.sd.dto.university.UniversityDto;
import ro.utcn.sd.entity.*;
import ro.utcn.sd.entity.enums.Status;

import java.util.Date;


public class ApplicationDto extends Dto {
    private String id;

    private Status status;

    private java.util.Date creationDate;

    private UniversityDto university;

    private Document document;

    private StudentDto student;

    private Address address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }



    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }


    public UniversityDto getUniversity() {
        return university;
    }

    public void setUniversity(UniversityDto university) {
        this.university = university;
    }

    public StudentDto getStudent() {
        return student;
    }

    public void setStudent(StudentDto student) {
        this.student = student;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
