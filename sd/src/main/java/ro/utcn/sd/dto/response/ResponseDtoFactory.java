package ro.utcn.sd.dto.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ro.utcn.sd.util.constants.Severity;
import ro.utcn.sd.dto.Dto;


public abstract class ResponseDtoFactory {
    public static ResponseEntity createErrorMessage(String message, HttpStatus status) {
        return ResponseEntity.status(status)
                .body(new ResponseDto(message, Severity.ERROR.toString()));
    }

    public static ResponseEntity createErrorMessage(String message) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ResponseDto(message, Severity.ERROR.toString()));
    }

    public static ResponseEntity createUnauthorizedMessage(String message) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
                .body(new ResponseDto(message, Severity.WARNING.toString()));
    }

    public static ResponseEntity createWarningMessage(String message, HttpStatus status) {
        return ResponseEntity.status(status)
                .body(new ResponseDto(message, Severity.WARNING.toString()));
    }

    public static ResponseEntity createSuccessMessage(String message, HttpStatus status, Dto dto) {
        return ResponseEntity.status(status)
                .body(new ResponseDto(message, Severity.SUCCESS.toString(), dto));
    }

    public static ResponseEntity createSuccessMessage(String message, Dto dto) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ResponseDto(message, Severity.SUCCESS.toString(), dto));
    }
}
