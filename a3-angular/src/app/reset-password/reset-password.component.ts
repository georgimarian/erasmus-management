import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/authentication.service';
import { MatSnackBar } from '@angular/material';
import { first } from 'rxjs/operators';
import { HttpService } from '../shared/http.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private http: HttpService,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar,
    private userService: AuthenticationService,
  ) {

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]
      ],
    });
  }

  get f() { return this.registerForm.controls; }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }


  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    this.http.post("reset", this.registerForm.value)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
          this.router.navigate(['/login']);
        },
        error => {
          this.openSnackBar(error.error.message, "Ok", 'style-error');
          this.loading = false;
        });
  }

}
