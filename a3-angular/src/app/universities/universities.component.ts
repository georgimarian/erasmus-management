import { Component, OnInit, ViewChild } from '@angular/core';
import { UniversityListService } from '../shared/university-list.service';
import { University } from '../shared/models/university.class';
import { TableService } from '../shared/table.service';
import { MatTableDataSource, MatDialog, MatPaginator, MatSort, MatSnackBar } from '@angular/material';
import { UpdateUniversityComponent } from '../update-university/update-university.component';
import { AddUniversityComponent } from '../add-university/add-university.component';
import { Router } from '@angular/router';
import { SessionService } from '../shared/session.service';


@Component({
  selector: 'universities',
  templateUrl: './universities.component.html',
  styleUrls: ['./universities.component.scss']
})


export class UniversitiesComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource;
  displayedColumns: string[];

  constructor(private universityService: UniversityListService,
    private sessionService: SessionService,
    private tableService: TableService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.refresh();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  createUniversity() {
    this.dialog.open(AddUniversityComponent, {
      data: {
        id: '',
        name: '',
        numberOfApplicants: '',
        teacher: ''
      }
    }).afterClosed().subscribe(() => this.refresh());;
  }

  updateUniversity(entry: University) {
    this.dialog.open(UpdateUniversityComponent, {
      // height: '400px',
      // width: '600px',
      data: {
        id: entry.id,
        name: entry.name,
        numberOfApplicants: entry.numberOfApplicants,
        teacher: entry.teacher
      }
    }).afterClosed().subscribe((data) => this.refresh());
  }

  viewDetails(entry: University) {
    this.router.navigate(['/university/'], { queryParams: { id: entry.id } });
  }

  refresh() {
    this.universityService.getAllUniversities().subscribe((data: any) => {
      console.log(data.dto);
      this.dataSource = new MatTableDataSource<University>(data.dto.universities as University[]);
      this.displayedColumns = this.tableService.validateKeys(Object.keys(data.dto.universities[0]));
      this.displayedColumns.push('actions');
      this.displayedColumns.unshift("index");
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, error => {
      this.openSnackBar(error.error.message, "Ok", 'style-error');
    });
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  deleteUniversity(entry: University) {
    this.universityService.deleteUniversity(entry.id).subscribe(
      (data: any) => {
        console.log(data);
        console.log(data.severity);
        this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
        this.refresh();
      },
      (error) => {
        console.log(error);
        this.openSnackBar(error.error.message, "Ok", 'style-error');
      }
    );
  }

}
