import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UniversitiesComponent } from './universities.component';
import { UniversitiesRoutingModule } from './universities-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatTableModule, MatPaginatorModule, MatFormFieldModule,
  MatInputModule, MatSortModule, MatIconModule, MatDialogModule, MatSelectModule, MatButtonModule, MatSnackBar, MatSnackBarModule, MatCardModule, MatTooltipModule
} from '@angular/material';
import { UpdateUniversityComponent } from '../update-university/update-university.component';
import { AddUniversityComponent } from '../add-university/add-university.component';

@NgModule({
  imports: [
    CommonModule,
    UniversitiesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule,
    FormsModule,
    MatInputModule,
    MatSortModule,
    MatDialogModule,
    MatSelectModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCardModule,
    MatTooltipModule
  ],
  declarations: [
    UniversitiesComponent,
    UpdateUniversityComponent,
    AddUniversityComponent
  ],
  entryComponents: [
    UpdateUniversityComponent,
    AddUniversityComponent
  ]

})
export class UniversitiesModule { }
