import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { HttpService } from '../shared/http.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-accommodation',
  templateUrl: './add-accommodation.component.html',
  styleUrls: ['./add-accommodation.component.scss']
})
export class AddAccommodationComponent implements OnInit {
  form: FormGroup;
  keys: any;
  originalData: any;
  id: number;
  displayData = false;
  rents = ["SHARED_DORM", "SINGLE_ROOM"];


  constructor(private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddAccommodationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private http: HttpService) { }

  ngOnInit() {

    let formData = {};

    this.id = this.data.id;
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
    });
    this.getEntryKeys();
    formData = this.getFormBuilderValues();

    this.form = this.formBuilder.group(
      formData
    );

    this.displayData = true;
  }

  getEntryKeys() {
    this.keys = ["name", "pricePerMonth", "distanceFromUniversity", "rentType"];
  }

  get f() { return this.form.controls; }

  positiveNumberValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const isNotOk = Number(control.value) < 0;
      return isNotOk ? { nonPositive: { value: control.value } } : null;
    };
  }

  getFormBuilderValues() {
    const formData = {};
    this.keys.forEach(key => {
      if (key !== 'id') {
        if (key === 'pricePerMonth' || key == "distanceFromUniversity") {
          return formData[key] = ['', [Validators.required, this.positiveNumberValidator()]];
        }
        return formData[key] = ['', Validators.required];
      }
    });

    if (formData.hasOwnProperty('id')) {
      delete formData['id'];
    }
    return formData;
  }

  close() {
    this.dialogRef.close();
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    console.log(this.form.value);
    this.http.post("accommodation/university/" + this.id + "/create", this.form.value).subscribe(
      (data: any) => {
        console.log(data);
        this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
        this.dialogRef.close();
      },
      (error) => {
        console.log(error);
        this.openSnackBar(error.error.message, "Ok", 'style-' + error.error.severity.toLowerCase());
      }
    );
  }

}
