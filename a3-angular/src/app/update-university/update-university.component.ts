import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { UniversityListService } from '../shared/university-list.service';
import { Router } from '@angular/router';
import { User } from '../shared/models/user.class';
import { Teacher } from '../shared/models/teacher.class';
import { TeacherService } from '../shared/teacher.service';

@Component({
  selector: 'app-update-university',
  templateUrl: './update-university.component.html',
  styleUrls: ['./update-university.component.scss']
})
export class UpdateUniversityComponent implements OnInit {
  form: FormGroup;
  keys: any;
  originalData: any;
  id: number;
  displayData = false;

  teachers: Array<Teacher> = [];

  constructor(private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UpdateUniversityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private snackBar: MatSnackBar,
    private service: UniversityListService,
    private teacherService: TeacherService) { }

  ngOnInit() {
    this.displayData = false;
    this.teacherService.getAllTeachers().subscribe((data: any) => {
      console.log(data)
      this.teachers = data.dto.teacherDtoList as Teacher[];
    });

  

    let formData = {};

    this.id = this.data.id;

    this.getEntryKeys();
    formData = this.getFormBuilderValues();

    this.form = this.formBuilder.group(
      formData
    );

    this.displayData = true;
  }

  getEntryKeys() {
    this.keys = Object.keys(this.data);
  }

  getFormBuilderValues() {
    const formData = {};
    this.keys.forEach(key => {
      if (key !== 'id') {
        return formData[key] = [this.data[key], Validators.required];
      }
    });

    if (formData.hasOwnProperty('id')) {
      delete formData['id'];
    }
    return formData;
  }

  close() {
    this.dialogRef.close();
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  save() {
    console.log(this.form.value);
    this.service.updateUniversity(this.id, this.form.value).subscribe(
      (data: any) => {
        console.log(data);
        this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
        this.dialogRef.close();
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
