import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})

export class UniversityListService {

  baseUrl = 'university/';

  constructor(private httpService: HttpService) { }

  getAllUniversities() {
    return this.httpService.getAll(this.baseUrl + 'all');
  }

  getUniversityById(id) {
    return this.httpService.getById(this.baseUrl, id);
  }

  updateUniversity(id, data) {
    return this.httpService.update(this.baseUrl + 'edit/', id, data);
  }

  deleteUniversity(id) {
    return this.httpService.delete(this.baseUrl + 'delete/' + id, id);
  }

  createUniversity(data) {
    return this.httpService.post(this.baseUrl + 'add/', data);
  }

}
