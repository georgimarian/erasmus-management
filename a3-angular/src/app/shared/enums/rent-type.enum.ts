export enum RentType {
    SHARED_DORM = 'SHARED_DORM',
    HOST_FAMILY = 'HOST_FAMILY',
    SINGLE_ROOM = 'SINGLE_ROOM'
  }
  