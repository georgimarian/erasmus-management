export enum Roles {
  ADMIN = 'admin',
  STUDENT = 'student',
  TEACHER = 'teacher'
}
