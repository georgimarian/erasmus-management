import { Injectable } from '@angular/core';
import { HttpService } from './http.service';


@Injectable({
  providedIn: 'root'
})
export class StudentService {


  baseUrl = 'student/';

  constructor(private httpService: HttpService) { }

  getAllStudents() {
    return this.httpService.getAll(this.baseUrl + 'all');
  }

  getStudentByUsername(username:string) {
    return this.httpService.getById(this.baseUrl + 'email/', username);
  }

  updateUser(id, data) {
    return this.httpService.update(this.baseUrl + 'edit/', id, data);
  }

  deleteUser(id) {
    return this.httpService.delete(this.baseUrl + 'delete/' + id, id);
  }

  createUser(data) {
    return this.httpService.post(this.baseUrl + 'create/', data);
  }
}
