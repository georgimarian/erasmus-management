import { Injectable } from '@angular/core';
import { HttpService } from './http.service';


@Injectable({
    providedIn: 'root'
})
export class MobilityService {


    baseUrl = 'mobility/';

    constructor(private httpService: HttpService) { }

    getAllMobilities() {
        return this.httpService.getAll(this.baseUrl + 'all');
    }

    getMobilityByUsername(username: string) {
        return this.httpService.getById(this.baseUrl + 'student/', username);
    }

    updateMobility(id, data) {
        return this.httpService.update(this.baseUrl + 'edit/', id, data);
    }

    deleteMobility(id) {
        return this.httpService.delete(this.baseUrl + 'delete/' + id, id);
    }

    createMobility(data) {
        return this.httpService.post(this.baseUrl + 'create/', data);
    }
}
