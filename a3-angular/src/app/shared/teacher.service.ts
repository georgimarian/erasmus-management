import { Injectable } from '@angular/core';
import { HttpService } from './http.service';


@Injectable({
  providedIn: 'root'
})
export class TeacherService {


  baseUrl = 'teacher/';

  constructor(private httpService: HttpService) { }

  getAllTeachers() {
    return this.httpService.getAll(this.baseUrl + 'all');
  }

  getTeacherById(id) {
    return this.httpService.getById(this.baseUrl, id);
  }

  updateTeacher(id, data) {
    return this.httpService.update(this.baseUrl + 'edit/', id, data);
  }

  deleteTeacher(id) {
    return this.httpService.delete(this.baseUrl + 'delete/' + id, id);
  }

  createTeacher(data) {
    return this.httpService.post(this.baseUrl + 'create/', data);
  }
}
