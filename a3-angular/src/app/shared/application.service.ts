import { Injectable } from '@angular/core';
import { HttpService } from './http.service';


@Injectable({
  providedIn: 'root'
})
export class ApplicationService {


  baseUrl = 'application/';

  constructor(private httpService: HttpService) { }

  getAllApplications() {
    return this.httpService.getAll(this.baseUrl + 'all');
  }

  getAllApplicationsByUser(role: string, email: string) {
    return this.httpService.getById(this.baseUrl + role + '/', email);
  }

  getApplicationById(id) {
    return this.httpService.getById(this.baseUrl, id);
  }

  updateApplication(id, data) {
    return this.httpService.update(this.baseUrl + 'edit/', id, data);
  }

  deleteApplication(id) {
    return this.httpService.delete(this.baseUrl + 'delete/' + id, id);
  }

  createApplication(data) {
    return this.httpService.post(this.baseUrl + 'create/', data);
  }
}
