import { Injectable } from '@angular/core';
import decode from 'jwt-decode';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

@Injectable()
export class SessionService {

  constructor(public auth: AuthenticationService, public router: Router, public jwtHelper: JwtHelperService) { }

  setSession(authResult) {
    const expiresAt = moment().add(decode(authResult).exp, 'second');
    localStorage.setItem('token_value', authResult);
    localStorage.setItem('role', decode(authResult).role);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('token_value');
    if (!token) {
      return false;
    }
    return !this.jwtHelper.isTokenExpired(token);
  }

  getUserType(): string {
    const token = localStorage.getItem('token_value');
    let tokenPayload;
    if (token !== '' && token !== null) {
      tokenPayload = decode(token);
      return tokenPayload.role;
    }
    // console.log(tokenPayload)
    return '';
  }

  getUser(): string {
    const token = localStorage.getItem('token_value');
    let tokenPayload;
    if (token !== '' && token !== null)
      tokenPayload = decode(token);
    // console.log(tokenPayload)
    return tokenPayload.sub || '';
  }

  getUserDetails(): string {
    const token = localStorage.getItem('token_value');
    const tokenPayload = decode(token);
    return tokenPayload.name;

  }

}
