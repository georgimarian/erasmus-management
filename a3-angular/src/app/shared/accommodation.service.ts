import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class AccommodationService {

  baseUrl = 'accommodation/';

  constructor(private httpService: HttpService,
    private http: HttpClient) { }

  getAllAccommodations() {
    return this.httpService.getAll(this.baseUrl + 'all');
  }

  getAccommodationById(id) {
    return this.httpService.getById(this.baseUrl, id);
  }

  getAccommodationByUniversityId(id) {
    return this.http.get('http://localhost:8080/api/accommodation/university/' + id);
  }

  updateAccommodation(id, data) {
    return this.httpService.update(this.baseUrl + 'edit/', id, data);
  }

  deleteAccommodation(id) {
    return this.httpService.delete(this.baseUrl + 'delete/' + id, id);
  }

  createAccommodation(data) {
    return this.httpService.post(this.baseUrl + 'add/', data);
  }

}
