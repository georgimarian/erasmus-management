import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class TableService {

  receivedData: any;
  keys: Array<string>;


  constructor() { }

  validateKeys(keys) {
    const seenKeys = keys;
    const index2 = keys.indexOf('id');
    if (index2 !== -1) {
      seenKeys.splice(index2, 1);
    }

    return seenKeys;
  }

}
