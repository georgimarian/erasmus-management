import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppConfig } from '../core/app.config';

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  baseUrl = "http://localhost:8080/api/";

  getAll(url) {
    return this.http.get(this.baseUrl + url);
  }

  getById(url, id) {
    return this.http.get(this.baseUrl + url + id);
  }

  post(url, data) {
    return this.http.post(this.baseUrl + url, data);
  }

  update(url, id, info) {
    return this.http.put(this.baseUrl + url + id, info);
  }

  delete(url, id) {
    return this.http.delete(this.baseUrl + url, id);
  }


  constructor(private http: HttpClient) { }

}
