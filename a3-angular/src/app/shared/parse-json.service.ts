import { Injectable } from '@angular/core';

declare var require: any;

@Injectable({
    providedIn: 'root'
})


export class ParseJsonService {

    constructor() { }

    convertJson(json) {
        if (json.ArrayList && json.ArrayList.item) {
            return json.ArrayList.item.map(el => this.convertJson(el));
        }

        if (json._text) {
            return json._text;
        }
        return Object.keys(json).reduce((acc, key) => ({
            ...acc,
            [key]: this.convertJson(json[key]),
        }), {});
    };

    parseJson(data) {
        var parser = require('xml-js');
        console.log(data)
        var json = parser.xml2json(data, { compact: true });
        var json1 = this.convertJson(JSON.parse(json));
        console.log(json);
        console.log(json1);
        return json1;
    }

    parseXML(data) {
        var parser = require('xml-js');
        var xml = parser.json2xml(data, { compact: true, spaces: 4 });
        console.log(xml);
        return xml;
    }

}
