import { RentType } from '../enums/rent-type.enum';
import { Address } from './address.class';

export class Accommodation{
    distanceFromUniversity: number;
    name: string;
    pricePerMonth: string;
    rentType: RentType;
    address: Address;
}