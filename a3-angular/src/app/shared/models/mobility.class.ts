import { Application } from './application.class';
import { AccommodationService } from '../accommodation.service';
import { Subject } from './subject.class';

export class Mobility{
    id: string;
    applicaion: Application;
    accommodation: AccommodationService;
    subjects: Subject[];
}