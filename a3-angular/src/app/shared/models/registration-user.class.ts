export class User {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    address: string;
  }
