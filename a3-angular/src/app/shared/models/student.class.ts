import { User } from './user.class';

export class Student{
    averageGrade: number;
    dateOfBirth: Date;
    yearOfStudy: number;
    user: User;
}