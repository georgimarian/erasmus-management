import { User } from './user.class';

export class Teacher{
    id: string;
    department: string;
    user: User;
}