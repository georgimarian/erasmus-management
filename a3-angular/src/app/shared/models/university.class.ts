import { Teacher } from './teacher.class';

export class University {
    id: number;
    name: string;
    numberOfApplicants: number;
    teacher: Teacher;
  }
