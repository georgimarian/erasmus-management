export class RegisterUser {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    averageGrade: number;
    yearOfStudy: number;
    dateOfBirth: Date;
  }
