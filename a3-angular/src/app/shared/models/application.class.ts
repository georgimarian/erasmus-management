import { University } from './university.class';
import { Student } from './student.class';
import { Address } from './address.class';

export class Application{
    id: string;
    creationDate: Date;
    document: Document;
    status: string;
    student: Student;
    university: University;
    address: Address;
}