import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { RoleGuardService } from './role-guard.service';
import { AuthGuardService } from './auth-guard.service';
import { SessionService } from './session.service';
import { HttpClient } from '@angular/common/http';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { ParseJsonService } from './parse-json.service';
import { MatTooltipModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [NavMenuComponent],
  exports: [
    CommonModule,
    RouterModule,
    NavMenuComponent
  ],
  imports: [
    RouterModule,
    CommonModule
  ],
  providers: [
    AuthenticationService,
    RoleGuardService,
    AuthGuardService,
    SessionService,
    ParseJsonService,
    MatTooltipModule,
    BrowserAnimationsModule
  ]
})
export class SharedModule { }
