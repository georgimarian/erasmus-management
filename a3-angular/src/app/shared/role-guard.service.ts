import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { SessionService } from './session.service';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(public sessionService: SessionService,
    public snackBar: MatSnackBar,
    public router: Router) { }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
      panelClass: [className]
    });
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const expectedRole: Array<string> = route.data.expectedRole;
    if (!expectedRole.includes(this.sessionService.getUserType())) {
      this.openSnackBar("You are not allowed on this page!", "Ok", 'style-error');
      this.router.navigate(['/home'])
      return false;
    }if(!this.sessionService.isAuthenticated()){
      this.openSnackBar("You are not allowed on this page!", "Ok", 'style-error');
      this.router.navigate(['/login'])
      return false;
    }
    return true;
  }

}
