import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from './models/user.class';
import { ParseJsonService } from './parse-json.service';
import { RegisterUser } from './models/register-user.class';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  baseUrl = 'http://localhost:8080/api/';

  constructor(private http: HttpClient,
    private parser: ParseJsonService,
    private router: Router) { }

  login(user: User) {
    localStorage.removeItem('token_value');
    console.log(user)
    let xmlUser = '<loginUserDto>\n' + this.parser.parseXML(user) + '\n</loginUserDto>'
    console.log(xmlUser)
    return this.http.post(this.baseUrl + 'login/', xmlUser,
      {
        headers: new HttpHeaders()
          .set('Accept', 'application/xml')
          .append('Content-type', 'application/xml')
          .append('Access-Control-Allow-Methods', 'POST')
          .append('Access-Control-Allow-Origin', '*')
          .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method"),
        responseType: 'text'
      });
  }

  register(user: RegisterUser) {
    console.log(user)
    return this.http.post(this.baseUrl + 'register', user);
  }

  reset(password) {
    return this.http.post(this.baseUrl + 'change-password', password);
  }

  signOut() {
    localStorage.removeItem('expires_at');
    localStorage.removeItem('token_value');
    localStorage.removeItem('role');
    this.router.navigate(['login']);
  }

}
