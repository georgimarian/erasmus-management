import { Component, OnInit, ɵConsole } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { University } from '../shared/models/university.class';
import { UniversityListService } from '../shared/university-list.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { ApplicationService } from '../shared/application.service';
import { SessionService } from '../shared/session.service';

@Component({
  selector: 'app-create-application',
  templateUrl: './create-application.component.html',
  styleUrls: ['./create-application.component.scss'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true }
  }]
})
export class CreateApplicationComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  universities: University[];
  years = [1, 2, 3, 4, 5, 6];
  semesters = [1, 2];

  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,
    private sessionService: SessionService,
    private universityService: UniversityListService,
    private applicationService: ApplicationService) { }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      year: ['', Validators.required],
      semester: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      university: ['', Validators.required]
    });
    this.universityService.getAllUniversities().subscribe((data: any) => {
      console.log(data)
      this.universities = data.dto.universities as University[];
      console.log(this.universities)
    },
      error => {
        console.log(error)
      });
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  complete() {
    if (this.firstFormGroup.valid && this.secondFormGroup.valid) {
      const result = Object.assign({}, this.firstFormGroup.value, this.secondFormGroup.value);
      result.student = this.sessionService.getUser();
      this.applicationService.createApplication(result).subscribe(data => {
        this.openSnackBar("Successfully created application", "OK", "style-success");
        this.router.navigate(["/applications"]);
      }, error => {
        this.openSnackBar("Error creating application", "OK", "style-error");
      });
    }
    else {
      this.openSnackBar("Invalid inputs for creating application", "OK", "style-error");
    }
  }

}
