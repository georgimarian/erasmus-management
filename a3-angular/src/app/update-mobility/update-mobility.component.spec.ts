import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMobilityComponent } from './update-mobility.component';

describe('UpdateMobilityComponent', () => {
  let component: UpdateMobilityComponent;
  let fixture: ComponentFixture<UpdateMobilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateMobilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMobilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
