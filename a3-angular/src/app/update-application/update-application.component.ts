import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { University } from '../shared/models/university.class';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { UpdateUniversityComponent } from '../update-university/update-university.component';
import { Router } from '@angular/router';
import { UniversityListService } from '../shared/university-list.service';
import { Status } from '../shared/enums/status.enum';
import { ApplicationService } from '../shared/application.service';

@Component({
  selector: 'app-update-application',
  templateUrl: './update-application.component.html',
  styleUrls: ['./update-application.component.scss']
})
export class UpdateApplicationComponent implements OnInit {
  form: FormGroup;
  keys: any;
  originalData: any;
  id: number;
  displayData = false;

  statuses: Array<string> = [];
  universities: University[];

  constructor(private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UpdateUniversityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private applicationService: ApplicationService,
    private snackBar: MatSnackBar,
    private universityService: UniversityListService) { }

  ngOnInit() {
    this.displayData = false;
    this.universityService.getAllUniversities().subscribe((data: any) => {
      console.log(data)
      this.universities = data.dto.universityDtoList as University[];
    });

    this.id = this.data.id;

    for (let value in Status) {
      this.statuses.push(value);
    }

    this.form = this.formBuilder.group(
      {
        id: [{ value: this.data.id, disabled: true }],
        creationDate: [{ value: this.data.creationDate, disabled: true }],
        document: [{ value: this.data.document.name, disabled: true }],
        status: this.data.status,
        student: [{ value: this.data.student, disabled: true }],
        university: [{ value: this.data.university, disabled: true }],
      }
    );

    this.displayData = true;
  }

  close() {
    this.dialogRef.close();
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  save() {
    console.log(this.form.value);
    let formData = {
      id: this.data.id,
      creationDate: this.data.creationDate,
      document: this.data.document,
      student: this.data.student,
      university: this.data.university,
      status: this.form.value.status
    }
    console.log(formData)
    this.applicationService.updateApplication(this.id, formData).subscribe(
      (data : any) => {
        console.log(data);
        this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
        this.dialogRef.close();
      },
      (error) => {
        console.log(error);
      }
    );
  }

}
