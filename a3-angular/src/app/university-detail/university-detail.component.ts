import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UniversityListService } from '../shared/university-list.service';
import { University } from '../shared/models/university.class';
import { Accommodation } from '../shared/models/accommodation.class';
import { AccommodationService } from '../shared/accommodation.service';
import { SessionService } from '../shared/session.service';
import { MatDialog } from '@angular/material';
import { AddAccommodationComponent } from '../add-accommodation/add-accommodation.component';

@Component({
  selector: 'app-university-detail',
  templateUrl: './university-detail.component.html',
  styleUrls: ['./university-detail.component.scss']
})
export class UniversityDetailComponent implements OnInit {
  id;
  data: University;
  accommodation: Accommodation[];

  constructor(private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private sessionService: SessionService,
    private universityService: UniversityListService,
    private accommodationService: AccommodationService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
    });
    this.universityService.getUniversityById(this.id).subscribe(
      (data: any) => {
        console.log(data);
        this.data = data.dto as University;
      },
      error => {
        console.log(error);
      }
    );
    this.accommodationService.getAccommodationByUniversityId(this.id).subscribe(
      (success: any) => {
        console.log(success);
        this.accommodation = success.dto.accommodationDtos as Accommodation[];
        console.log(this.accommodation)
      },
      error => {
        console.log(error)
      });

  }

  back() {
    this.router.navigate(['/universities']);
  }

  refresh(){

  }

  add(){
    this.dialog.open(AddAccommodationComponent, {
      data: {
        id: '',
        name: '',
        numberOfApplicants: '',
        teacher: ''
      }
    }).afterClosed().subscribe(() => this.refresh());;
  }

}
