import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginModule } from './login/login.module';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { JwtModule } from '@auth0/angular-jwt';
import { AppConfig } from './core/app.config';
import { UniversitiesModule } from './universities/universities.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule, MatSelectModule, MatDialogModule, MatSidenavModule, MatButtonModule, MatSnackBarModule, MatCardModule, MatIconModule, MatPaginator, MatPaginatorModule, MatSortModule, MatFormFieldControl, MatInputModule, MatTooltipModule, MatStepperModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { ApplicationsComponent } from './applications/applications.component';
import { MobilitiesComponent } from './mobilities/mobilities.component';
import { UniversityDetailComponent } from './university-detail/university-detail.component';
import { LogoutComponent } from './logout/logout.component';
import { TeachersComponent } from './teachers/teachers.component';
import { UpdateApplicationComponent } from './update-application/update-application.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthInterceptor } from './shared/authinterceptor.service';
import { RegisterComponent } from './register/register.component';
import { CreateApplicationComponent } from './create-application/create-application.component';
import { AddMobilityComponent } from './add-mobility/add-mobility.component';
import { ContactTeacherComponent } from './contact-teacher/contact-teacher.component';
import { UpdateMobilityComponent } from './update-mobility/update-mobility.component';
import { MobilityDetailComponent } from './mobility-detail/mobility-detail.component';
import { TeacherDetailComponent } from './teacher-detail/teacher-detail.component';
import { UpdateTeacherComponent } from './update-teacher/update-teacher.component';
import { AddAccommodationComponent } from './add-accommodation/add-accommodation.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ResetCodeComponent } from './reset-code/reset-code.component';


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    HomeComponent,
    ApplicationsComponent,
    MobilitiesComponent,
    UniversityDetailComponent,
    LogoutComponent,
    TeachersComponent,
    UpdateApplicationComponent,
    ApplicationDetailsComponent,
    ProfileComponent,
    RegisterComponent,
    CreateApplicationComponent,
    AddMobilityComponent,
    ContactTeacherComponent,
    UpdateMobilityComponent,
    MobilityDetailComponent,
    TeacherDetailComponent,
    UpdateTeacherComponent,
    AddAccommodationComponent,
    ResetPasswordComponent,
    ResetCodeComponent
  ],
  imports: [
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatTableModule,
    MatSelectModule,
    MatDialogModule,
    BrowserModule,
    LoginModule,
    MatSnackBarModule,
    UniversitiesModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    MatSidenavModule,
    MatButtonModule,
    MatCardModule,
    MatIconModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTooltipModule,
    MatStepperModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return localStorage.getItem('token_value');
        },
        whitelistedDomains: AppConfig.getConfig('whitelistedDomains')
      }
    }),
  ],
  providers: [
    AppConfig, {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    UpdateApplicationComponent,
    AddAccommodationComponent
  ]
})
export class AppModule { }
