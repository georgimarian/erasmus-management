import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { UniversityListService } from '../shared/university-list.service';
import { TableService } from '../shared/table.service';
import { Router } from '@angular/router';
import { AddUniversityComponent } from '../add-university/add-university.component';
import { Teacher } from '../shared/models/teacher.class';
import { UpdateUniversityComponent } from '../update-university/update-university.component';
import { TeacherService } from '../shared/teacher.service';
import { SessionService } from '../shared/session.service';
import { UpdateTeacherComponent } from '../update-teacher/update-teacher.component';

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource;
  displayedColumns: string[];

  constructor(private teacherService: TeacherService,
    private tableService: TableService,
    private snackBar: MatSnackBar,
    private sessionService: SessionService,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.refresh();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  addTeacher() {
    this.openSnackBar("Someday you will add a teacher, but not today :) !", "Ok", 'style-error');
    // this.dialog.open(AddUniversityComponent, {
    //   data: {
    //     id: '',
    //     name: '',
    //     numberOfApplicants: '',
    //     teacher: ''
    //   }
    // }).afterClosed().subscribe(() => this.refresh());;
  }

  updateTeacher(entry: Teacher) {
    this.dialog.open(UpdateTeacherComponent, {
      // height: '400px',
      // width: '600px',
      data: {
        name: entry.department,
      }
    }).afterClosed().subscribe(() => this.refresh());
    this.openSnackBar("Cannot edit yet! :O ", "Ok", 'style-error')

  }

  sendEmail(entry: Teacher) {
    this.router.navigate(['/contact-teacher/'], { queryParams: { id: entry.id } });
  }



  viewDetails(entry: Teacher) {
    this.router.navigate(['/teacher/'], { queryParams: { id: entry.id } });
    this.openSnackBar("Cannot see details yet! :O ", "Ok", 'style-error')

  }

  refresh() {
    this.teacherService.getAllTeachers().subscribe((data: any) => {
      console.log(data);
      this.dataSource = new MatTableDataSource<Teacher>(data.dto.teacherDtoList as Teacher[]);
      this.displayedColumns = this.tableService.validateKeys(Object.keys(data.dto.teacherDtoList[0]));
      this.displayedColumns.push('actions');
      this.displayedColumns.unshift("index");
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  deleteTeacher(entry: Teacher) {
    this.teacherService.deleteTeacher(entry.id).subscribe(
      (data: any) => {
        console.log(data);
        console.log(data.severity);

        this.refresh();
      },
      (error) => {
        console.log(error)
        console.log(error.severity);

        this.openSnackBar(error.error.message, "Ok", 'style-error')
      }
    );
  }

}
