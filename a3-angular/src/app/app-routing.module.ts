import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RoleGuardService } from './shared/role-guard.service';
import { Roles } from './shared/enums/roles.enum';
import { HomeComponent } from './home/home.component';
import { UniversityDetailComponent } from './university-detail/university-detail.component';
import { LogoutComponent } from './logout/logout.component';
import { TeachersComponent } from './teachers/teachers.component';
import { ApplicationsComponent } from './applications/applications.component';
import { MobilitiesComponent } from './mobilities/mobilities.component';
import { AuthGuardService } from './shared/auth-guard.service';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import { CreateApplicationComponent } from './create-application/create-application.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { ContactTeacherComponent } from './contact-teacher/contact-teacher.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { TeacherDetailComponent } from './teacher-detail/teacher-detail.component';
import { ResetCodeComponent } from './reset-code/reset-code.component';

const routes: Routes = [
  {
    path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
  },
  {
    path: 'universities', loadChildren: () => import('./universities/universities.module').then(m => m.UniversitiesModule),
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER]
    }
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home', component: HomeComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER]
    }
  },
  {
    path: 'university', component: UniversityDetailComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER]
    }
  },
  {
    path: 'teachers', component: TeachersComponent, canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT]
    }
  },
  {
    path: 'teacher', component: TeacherDetailComponent, canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT]
    }
  },
  {
    path: 'logout', component: LogoutComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER]
    }
  },
  {
    path: 'applications', component: ApplicationsComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER]
    }
  },
  {
    path: 'application', component: ApplicationDetailsComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER]
    }
  },
  {
    path: 'contact-teacher', component: ContactTeacherComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT]
    }
  },
  {
    path: 'mobilities', component: MobilitiesComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.ADMIN, Roles.STUDENT, Roles.TEACHER]
    }
  },
  {
    path: 'profile', component: ProfileComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.STUDENT]
    }
  },
  {
    path: 'apply', component: CreateApplicationComponent,
    canActivate: [RoleGuardService, AuthGuardService],
    data: {
      expectedRole: [Roles.STUDENT]
    }
  },
  { path: 'reset-code', component: ResetCodeComponent },
  { path: 'reset', component: ResetPasswordComponent },
  { path: 'register', component: RegisterComponent },
  { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
