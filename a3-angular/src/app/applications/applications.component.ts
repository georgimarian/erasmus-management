import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSnackBar, MatDialog, MatTableDataSource } from '@angular/material';
import { TableService } from '../shared/table.service';
import { Router } from '@angular/router';
import { Application } from '../shared/models/application.class';
import { ApplicationService } from '../shared/application.service';
import { UpdateApplicationComponent } from '../update-application/update-application.component';
import { SessionService } from '../shared/session.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})
export class ApplicationsComponent implements OnInit {


  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource;
  displayedColumns: string[];
  applicationsNumber: number;

  constructor(private applicationService: ApplicationService,
    private tableService: TableService,
    private sessionService: SessionService,
    private snackBar: MatSnackBar,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.refresh();
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  updateApplication(entry: Application) {
    this.dialog.open(UpdateApplicationComponent, {
      // height: '400px',
      // width: '600px',
      data: {
        id: entry.id,
        creationDate: entry.creationDate,
        document: entry.document,
        status: entry.status,
        student: entry.student,
        university: entry.university,
        address: entry.address
      }
    }).afterClosed().subscribe(() => this.refresh());
  }


  viewDetails(entry: Application) {
    this.router.navigate(['/application/'], { queryParams: { id: entry.id } });
  }

  createApplication() {
    if (this.applicationsNumber < 3)
      this.router.navigate(["/apply"]);
    else
      this.openSnackBar("You have exceeded the applications for this year! :O ", "Ok", 'style-error')

  }

  refresh() {
    if (this.sessionService.getUserType() === 'admin')
      this.applicationService.getAllApplications().subscribe((data: any) => {
        console.log(data);
        this.dataSource = new MatTableDataSource<Application>(data.dto.applicationDtoList as Application[]);
        this.displayedColumns = this.tableService.validateKeys(Object.keys(data.dto.applicationDtoList[0]));
        this.displayedColumns.pop();
        this.displayedColumns.push('actions');
        this.displayedColumns.unshift("index");
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    else
      this.applicationService.getAllApplicationsByUser(this.sessionService.getUserType(), this.sessionService.getUser()).subscribe((data: any) => {
        console.log(data);
        let applications = data.dto.applicationDtoList as Application[];

        this.applicationsNumber = applications.filter(e =>
          new Date(e.creationDate).getFullYear() === new Date().getFullYear()
        ).length;

        this.dataSource = new MatTableDataSource<Application>(data.dto.applicationDtoList as Application[]);
        if (data.dto.applicationDtoList) {
          this.displayedColumns = this.tableService.validateKeys(Object.keys(data.dto.applicationDtoList[0]));
          this.displayedColumns.pop();
          this.displayedColumns.push('actions');
          this.displayedColumns.unshift("index");
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      });
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  deleteApplication(entry: Application) {
    this.applicationService.deleteApplication(entry.id).subscribe(
      (data: any) => {
        console.log(data);
        this.openSnackBar(data.message, "Ok", 'style-' + data.severity)
        this.refresh();
      },
      (error) => {
        console.log(error)
        console.log(error.severity);

        this.openSnackBar(error.error.message, "Ok", 'style-error')
      }
    );
  }

}
