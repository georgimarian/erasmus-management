import { ajax } from 'rxjs/ajax';
import { map, flatMap, switchMap } from 'rxjs/operators';
import { throwError } from 'rxjs';

// @dynamic
export class AppConfig {
  private static config: any = {};

  constructor() { }

  static getConfig(key: any) {
    return AppConfig.config[key];
  }

  static loadLocalConfigs() {
    return new Promise((resolve, reject) => {
      ajax(window.location.origin + '/configs.json?' + new Date().getTime())
        .pipe(
          map(res => res.response)
        )
        .subscribe(
          (localConfig) => {
            AppConfig.config = localConfig;

            resolve();
          },
          (error) => {
            console.error('Configuration file "configs.json" could not be read', error.message);

            reject();

            return throwError(error.message);
          }
        );
    });
  }
}
