import { Component, OnInit, ViewChild } from '@angular/core';
import { SessionService } from '../shared/session.service';
import { StudentService } from '../shared/student.service';
import { Student } from '../shared/models/student.class';
import { Router } from '@angular/router';
import { MobilityService } from '../shared/mobility.service';
import { Mobility } from '../shared/models/mobility.class';
import { MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  dataSource;
  displayedColumns: string[];

  user: string;
  student: Student;
  username: string;
  mobilities;
  constructor(private sessionService: SessionService,
    private router: Router,
    private mobilityService: MobilityService,
    private studentService: StudentService) { }

  back() {
    this.router.navigate(['/home']);
  }

  ngOnInit() {
    this.user = this.sessionService.getUserDetails();
    let username = this.sessionService.getUser();
    this.username = username;
    this.studentService.getStudentByUsername(username).subscribe(
      (success: any) => {
        console.log(success);
        this.student = success.dto as Student;
        console.log(this.student)
      },
      error => {
        console.log(error)
      });
  }

  getMobilities() {
    this.mobilityService.getMobilityByUsername(this.username).subscribe(
      (success: any) => {
        console.log(success);
        this.mobilities = success.dto.mobilityDtoList as Mobility;
        console.log(this.mobilities)
      },
      error => {
        console.log(error)
      });

  }

}
