import { Component, OnInit } from '@angular/core';
import { ApplicationService } from '../shared/application.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Application } from '../shared/models/application.class';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.scss']
})
export class ApplicationDetailsComponent implements OnInit {
  id;
  data: Application;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private applicationService: ApplicationService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
    });
    this.applicationService.getApplicationById(this.id).subscribe(
      (data: any) => {
        console.log(data);
        this.data = data.dto as Application;
      },
      error => {
        console.log(error)
      }
    );
  }

  back(){
    this.router.navigate(['/applications']);
  }

}
