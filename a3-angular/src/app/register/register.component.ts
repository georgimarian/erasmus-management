import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/authentication.service';
import { first } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar,
    private userService: AuthenticationService,
  ) {
    // redirect to home if already logged in
    // if (this.authenticationService.currentUserValue) { 
    //     this.router.navigate(['/']);
    // }
  }

  positiveNumberValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const isNotOk = Number(control.value) < 0;
      return isNotOk ? { nonPositive: { value: control.value } } : null;
    };
  }

  pastDateValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const date = new Date(control.value).getFullYear();
      let isNotOk;
      if (date > 2005) {
        isNotOk = true;
      }
      else { isNotOk = false; }
      return isNotOk ? { nonPositive: { value: control.value } } : null;
    };
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      yearOfStudy: ['', [Validators.required, this.positiveNumberValidator()]],
      averageGrade: ['', [Validators.required, this.positiveNumberValidator()]],
      dateOfBirth: ['', [Validators.required, this.pastDateValidator()]],
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]
      ],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registerForm.controls; }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }


  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    console.log(this.registerForm.value)
    this.loading = true;
    this.userService.register(this.registerForm.value)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
          this.router.navigate(['/login']);
        },
        error => {
          this.openSnackBar(error.error.message, "Ok", 'style-error');
          this.loading = false;
        });
  }

}
