import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormGroup, FormBuilder, Validator } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { UniversityListService } from '../shared/university-list.service';
import { Teacher } from '../shared/models/teacher.class';
import { TeacherService } from '../shared/teacher.service';

@Component({
  selector: 'app-add-university',
  templateUrl: './add-university.component.html',
  styleUrls: ['./add-university.component.scss']
})
export class AddUniversityComponent implements OnInit {

  form: FormGroup;
  keys: any;
  originalData: any;
  id: number;
  displayData = false;


  teachers: Teacher[];

  constructor(private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<AddUniversityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private snackBar: MatSnackBar,
    private service: UniversityListService,
    private teacherService: TeacherService) { }

  ngOnInit() {
    this.displayData = false;
    this.teacherService.getAllTeachers().subscribe((data: any) => {
      this.teachers = data.dto.teacherDtoList as Teacher[];
    });

    let formData = {};

    this.id = this.data.id;

    this.getEntryKeys();
    formData = this.getFormBuilderValues();

    this.form = this.formBuilder.group(
      formData
    );

    this.displayData = true;
  }

  getEntryKeys() {
    this.keys = Object.keys(this.data);
  }

  get f() { return this.form.controls; }


  getFormBuilderValues() {
    const formData = {};
    this.keys.forEach(key => {
      if (key !== 'id') {
        return formData[key] = [this.data[key], Validators.required];
      }
    });

    if (formData.hasOwnProperty('id')) {
      delete formData['id'];
    }
    return formData;
  }

  close() {
    this.dialogRef.close();
  }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    console.log(this.form.value);
    this.service.createUniversity(this.form.value).subscribe(
      (data: any) => {
        console.log(data);
        this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
        this.dialogRef.close();
      },
      (error) => {
        console.log(error);
        this.openSnackBar(error.error.message, "Ok", 'style-' + error.error.severity.toLowerCase());
      }
    );
  }
}
