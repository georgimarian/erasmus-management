import { Component, OnInit } from '@angular/core';
import { Teacher } from '../shared/models/teacher.class';
import { ActivatedRoute, Router } from '@angular/router';
import { ApplicationService } from '../shared/application.service';
import { Application } from '../shared/models/application.class';
import { TeacherService } from '../shared/teacher.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpService } from '../shared/http.service';
import { MatSnackBar } from '@angular/material';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { CustomMessage } from '../shared/models/custom-message.class';

@Component({
  selector: 'app-contact-teacher',
  templateUrl: './contact-teacher.component.html',
  styleUrls: ['./contact-teacher.component.scss']
})
export class ContactTeacherComponent implements OnInit {

  id;
  data: Teacher;
  formGroup: FormGroup;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private http: HttpService,
    private formBuilder: FormBuilder,
    private teacherService: TeacherService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
    });
    this.teacherService.getTeacherById(this.id).subscribe(
      (data: any) => {
        console.log(data);
        this.data = data.dto as Teacher;
      },
      error => {
        console.log(error)
      }
    );
    this.formGroup = this.formBuilder.group({
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  get f() { return this.formGroup.controls; }

  back() {
    this.router.navigate(['/teachers']);
  }

  submitted = false;
  error: {};

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }

  send() {
    if (this.formGroup.invalid) {
      return;
    }
    this.submitted = true;
    let message = new CustomMessage();
    message = this.formGroup.value;
    console.log(message);
    this.http.post('teacher/contact/' + this.id, message).subscribe(
      data => {
        this.openSnackBar("Successfully sent e-mail", "OK", "style-success");
        setTimeout(() => this.router.navigate(["/teachers"]),2000);
        console.log(data);
      },
      error => {
        console.log(error);
      }
    );
    // return this.cmspageService.contactForm(this.model).subscribe(
    //   data => console.log(data0),
    //   error => this.error = error
    // );
  }


}
