import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../shared/authentication.service';
import { SessionService } from '../shared/session.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../shared/models/user.class';
import { ParseJsonService } from '../shared/parse-json.service';
import { MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent {

  constructor(private router: Router,
    private loginService: AuthenticationService,
    private parser: ParseJsonService,
    private sessionService: SessionService,
    private formBuilder: FormBuilder,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute
  ) {
    if (this.sessionService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.loginForm.controls; }

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }


  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    let user: User = new User();
    user.email = this.f.username.value;
    user.password = this.f.password.value;
    this.loginService.login(user).subscribe(
      (data: any) => {
        console.log(data);
        let newData = this.parser.parseJson(data);
        console.log(newData.ResponseDto);
        this.sessionService.setSession(newData.ResponseDto.dto.message);
        this.router.navigate(['/home']);
      },
      error => {
        let newData = this.parser.parseJson(error.error);
        console.log(newData)
        this.openSnackBar(newData.ResponseDto.message, "Ok", 'style-error');
        this.loading = false;
      });
  }

}
