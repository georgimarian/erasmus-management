import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobilitiesComponent } from './mobilities.component';

describe('MobilitiesComponent', () => {
  let component: MobilitiesComponent;
  let fixture: ComponentFixture<MobilitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobilitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobilitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
