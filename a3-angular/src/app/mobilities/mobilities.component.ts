import { Component, OnInit } from '@angular/core';
import { Mobility } from '../shared/models/mobility.class';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mobilities',
  templateUrl: './mobilities.component.html',
  styleUrls: ['./mobilities.component.scss']
})
export class MobilitiesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  viewDetails(entry: Mobility) {
    this.router.navigate(['/university/'], { queryParams: { id: entry.id } });
  }

}
