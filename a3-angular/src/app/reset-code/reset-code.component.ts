import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../shared/authentication.service';
import { MatSnackBar } from '@angular/material';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-reset-code',
  templateUrl: './reset-code.component.html',
  styleUrls: ['./reset-code.component.scss']
})
export class ResetCodeComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar,
    private userService: AuthenticationService,
  ) {
    // redirect to home if already logged in
    // if (this.authenticationService.currentUserValue) { 
    //     this.router.navigate(['/']);
    // }
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]
      ],
      code: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password2: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registerForm.controls; }

  openSnackBar(message: string, action: string, className: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: [className]
    });
  }


  onSubmit() {
    this.submitted = true;
    console.log(this.registerForm.value['password'])
    if (this.registerForm.invalid || this.registerForm.value['password'] !== this.registerForm.value['password2']) {
      this.openSnackBar("Invalid form data!", "OK", 'style-error');
      return;
    }

    this.loading = true;
    this.userService.reset(this.registerForm.value)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.openSnackBar(data.message, "Ok", 'style-' + data.severity.toLowerCase());
          this.router.navigate(['/login']);
        },
        error => {
          this.openSnackBar(error.error.message, "Ok", 'style-error');
          this.loading = false;
        });
  }

}
