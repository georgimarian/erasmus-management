# README #

This is a semester project for the subject Software Desing, which I studied in my 3rd year at TUCN 

### Summary ###

* Project Description
* Backend Solution
* Frontend Solution
* Usage Guide

### Project Description ###

The idea for this project's topic emerged after having spent a semester abroad on an Erasmus+ scholarship. I found the whole paperwork for the departure process overwhelming and I felt the need to simplify the structure for future students.
I designed the application bearing in mind all the things I felt a real-life application process lacked.
* My applications (a student can have 3 applications per schoolyear)
* Direct e-mail sending to teachers
* List of universities
* Status of applications

### Backend Solution ###

* Maven Project
* Java Spring Boot
* JUnit tests 
* Mock tests

### Frontend Solution ###

* Angular 7 
* Material Design
* Separate components for each entity

### Usage Guide ###

* link and credentials will be provided
